<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Domain\Front\Request\InquiryRequest;
use App\Http\Controllers\Controller;
use App\Models\Inquiry;
use Mail;
use App\Mail\InquiryAdminMail;
use App\Mail\InquiryUserMail;
use Illuminate\Support\Arr;
use App\Jobs\SendEmailJob;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InquiryRequest $request)
    {
        $input=$request->persist();
        $input2 = Arr::except($input,['attachment_file']);
        if( Inquiry::create($input2)){
            Mail::to($request->email)->send(new InquiryUserMail($input));
            Mail::to(env('MAIL_USERNAME'))->send(new InquiryAdminMail($input));
             return response()->json([
                'success' => true,
                'message' => 'User Added Successfully.',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
