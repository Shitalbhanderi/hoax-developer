<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\ContactUs;
use App\Models\Home;
use App\Models\Portfolio;
use App\Models\Services;
use App\Models\SocialMediaIcon;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
    public function index()
    {
        $blogs = Blog::all();
        $home = Home::first();
        $portfolios = Portfolio::all();
        $services = Services::all();
        $contact = ContactUs::first();
        $sociamedias = SocialMediaIcon::all();
        return view('front.index', compact('blogs', 'home', 'portfolios', 'services', 'contact', 'sociamedias'));
    }
    public function blogReadMore($id)
    {
        $blog = Blog::findorfail($id);
        $blogs = Blog::whereNotIn('id', [$id])->limit(3)->get();
        $contact = ContactUs::first();

        return view('front.blog.readmore', compact('contact', 'blog', 'blogs'));
    }
    public function blogViewAll()
    {
        $blogs = Blog::all();
        $contact = ContactUs::first();
        return view('front.blog.viewall', compact('blogs', 'contact'));
    }
    public function privacypolicy()
    {
        return view('front.privacypolicy.privacypolicy');
    }
    public function termsconditions()
    {
        return view('front.privacypolicy.termcondition');
    }
}
