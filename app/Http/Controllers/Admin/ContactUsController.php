<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Datatables\InquiryDatatableScope;
use Illuminate\Http\Request;
use App\Domain\Admin\Request\ContactUsRequest;
use App\Domain\Admin\Request\SocialMediaRequest;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use App\Models\Services;
use App\Models\SocialMediaIcon;
use Illuminate\Support\Arr;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inquieryIndex(InquiryDatatableScope $datatable)
    {
        view()->share('route', 'inquiry');

        if (request()->ajax()) {
            return $datatable->query();
        }

        return view('admin.contactus.inquiry', [
            'html' => $datatable->html(),
        ]);
    }

    public function index()
    {
        view()->share('route', 'contactus');
        $contact = ContactUs::first();
        $sociamedias = SocialMediaIcon::all();
        $services = Services::all();
        return view('admin.contactus.index', compact('contact', 'sociamedias', 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ContactUsRequest $request)
    { }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->input('id');
        $contact = ContactUs::find($id);
        $output = [
            'logo' => $contact->logo,
            'footer_text' => $contact->footer_text,
            'skype_id' => $contact->skype_id,
        ];
        echo json_encode($output);
    }

    public function showSocialMedia(Request $request)
    {
        $socialmedias['data'] = SocialMediaIcon::all();
        echo json_encode($socialmedias);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ContactUsRequest $request, $id)
    {
        $input = $request->persist();
        $contactus = ContactUs::find($id);
        $image = $contactus->logo;
        if (array_key_exists('logo', $input) && file_exists('uploads/contactus/' . $image) && $image != '') {
            unlink('uploads/contactus/' . $image);
        }
        if ($contactus->fill($input)->save()) {
            return response()->json([
                'success' => true,
                'message' => 'User Added Successfully.',
            ]);
        }
    }

    public function updatesocialmedia(SocialMediaRequest $srequest)
    {
        foreach ($srequest->persist() as $value) {
            if ($value) {
                if ($value['id']) {
                    $socialmedia = SocialMediaIcon::find($value['id']);
                    $image = $socialmedia->icon;
                    if (array_key_exists('icon', $value) && file_exists(public_path('uploads/socialmedia/' . $image)) && $image != ' ') {
                        unlink(public_path('uploads/socialmedia/' . $image));
                    }
                }
                $input = Arr::except($value, ['id']);
                SocialMediaIcon::updateOrCreate(['id' => $value['id']], $input);
            }
        }
        return response()->json([
            'success' => true,
            'message' => 'User Added Successfully.',
        ]);
    }

    public function deletesocialmedia($id)
    {
        $socialmedia = SocialMediaIcon::find($id);
        ($socialmedia) ? $image = $socialmedia->icon : $image = ' ';
        if (file_exists(public_path('uploads/socialmedia/' . $image)) && $image != '') {
            unlink(public_path('uploads/socialmedia/' . $image));
        }
        SocialMediaIcon::whereId($id)->delete();

        return response()->json([
            'success' => 'Record deleted successfully!',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { }
}
