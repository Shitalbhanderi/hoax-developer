<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Request\EmailRequest;
use App\Domain\Util\MailUtility;
use App\Http\Controllers\Controller;
use App\Models\AdminUser;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('admin.auth.forgot-password');
    }

    public function sendResetLinkEmail(EmailRequest $request)
    {
        $user = AdminUser::whereEmail($request->get('email'))->first();
        $token = str_random(20) . time();
        $user->fill([
            'forgot_password_token' => $token
        ])->save();

        $data = [
            'name' => $user->username,
            'link' => route('admin:reset:password', $token)
        ];
        (new MailUtility())->sendMail('admin.emails.forgot-password', $data, $request->get('email'), 'Forgot Password.');
        session()->flash('success', 'Reset password link sent to your email.');

        return redirect()->route('admin:login');
    }
}
