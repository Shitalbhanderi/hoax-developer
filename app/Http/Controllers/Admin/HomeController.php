<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Domain\Admin\Request\HomeRequest;
use App\Http\Controllers\Controller;
use App\Models\Home;
use Illuminate\Support\Arr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        view()->share('route', 'home');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home = Home::all()->first();
        return view('admin.home.index', compact('home'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->input('id');
        $home = Home::find($id);
        $output = [
            'head_title' => $home->head_title,
            'hero_description' => $home->hero_description,
            'header_button' =>  $home->header_button,
            'header_button_link' => $home->header_button_link,
            'intro_img' => $home->intro_img
        ];
        echo json_encode($output);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request,$id){
    //     dd($request);
    // }

    public function update(HomeRequest $request, $id)
    {
        $input = $request->persist();
        $home = Home::find($id);
        $header_image = $home->header_button;
        $intro_image = $home->intro_img;
        if (array_key_exists('header_button', $input) && file_exists('uploads/home/' . $header_image) && $header_image != "") {
            unlink('uploads/home/' . $header_image);
        }
        if (array_key_exists('intro_img', $input) && file_exists('uploads/home/' . $intro_image) && $intro_image != "") {
            unlink('uploads/home/' . $intro_image);
        }

        if ($home->fill($input)->save()) {
            return response()->json([
                'success' => true,
                'message' => 'User Added Successfully.',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
