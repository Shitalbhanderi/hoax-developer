<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Domain\Admin\Request\BlogsRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use App\Models\Blog;
use App\Models\ContactUs;


class BlogsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        view()->share('route', 'blog');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        $contact = ContactUs::first();
        return view('admin.blogs.index', compact('blogs'));
    }

    public function blogReadMore($id)
    {
        $blog = Blog::findorfail($id);
        $blogs = Blog::whereNotIn('id', [$id])->limit(3)->get();
        $contact = ContactUs::first();
        return view('front.blog.readmore', compact('contact', 'blog', 'blogs'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blog = Blog::First();
        $output = [
            'section_header' => $blog->section_header,
            'section_title' => $blog->section_title
        ];
        echo json_encode($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogsRequest $request)
    {
        $input = $request->persist();
        if (Blog::create($request->persist())) {
            return response()->json([
                'success' => true,
                'message' => 'User Added Successfully.',
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->input('id');
        $blog = Blog::find($id);
        $output = [
            'section_header' => $blog->section_header,
            'section_title' => $blog->section_title,
            'blog_item_content_heading' => $blog->blog_item_content_heading,
            'blog_item_content_description' => $blog->blog_item_content_description,
            'auther' => $blog->auther
        ];
        echo json_encode($output);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlogsRequest $request, $id)
    {

        $input = $request->persist();
        $blog = Blog::find($id);
        $image = $blog->blog_item_image;
        if (array_key_exists('blog_item_image', $input) && file_exists('uploads/blogs/' . $image) && $image != "") {
            unlink('uploads/blogs/' . $image);
        }
        if ($blog->fill($input)->save()) {
            return response()->json([
                'success' => true,
                'message' => 'User Added Successfully.',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Blog::count() > 1) {
            $blog = Blog::find($id);
            ($blog) ? $image = $blog->blog_item_image : $image = ' ';
            if (file_exists(public_path('/uploads/blogs/blog-medium/' . $image)) && $image != '') {
                unlink(public_path('/uploads/blogs/blog-medium/' . $image));
            }
            if (file_exists(public_path('/uploads/blogs/blog-big/' . $image)) && $image != '') {
                unlink(public_path('/uploads/blogs/blog-big/' . $image));
            }
            if (file_exists(public_path('//uploads/blogs/blog-small/' . $image)) && $image != '') {
                unlink(public_path('//uploads/blogs/blog-small/' . $image));
            }
            if (Blog::whereId($id)->delete()) {
                return response()->json([
                    'data' => 'success',
                ]);
            }
        } else {
            return response()->json([
                'data' => 'fail',
            ]);
        }
    }
}
