<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Admin\Request\PortfolioRequest;
use App\Models\Portfolio;
use Illuminate\Support\Arr;

class PortfolioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        view()->share('route', 'portfolio');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolios = Portfolio::all();
        return view('admin.portfolio.index', compact('portfolios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id = $request->input('id');
        $portfolio = Portfolio::find($id);

        $output = [
            'section_header' => $portfolio->section_header,
            'section_title' => $portfolio->section_title,
            'portfolio_item_name' =>  $portfolio->portfolio_item_name,
            'android' => $portfolio->portfolio_item_andriod,
            'portfolio_item_andriod_url' => $portfolio->portfolio_item_andriod_url,
            'portfolio_item_ios_url' => $portfolio->portfolio_item_ios_url,
            'ios' => $portfolio->portfolio_item_ios,
            'portfolio_item_name_color'=>$portfolio->portfolio_item_name_color
        ];
        echo json_encode($output);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PortfolioRequest $request, $id)
    {

        $input = $request->persist();
        $portfolio = Portfolio::find($id);
        
        $image = $portfolio->portfolio_item_image;
        if (array_key_exists('portfolio_item_image', $input) && file_exists('uploads/portfolio/big/' . $image) && file_exists('uploads/portfolio/small/' . $image) && $image != "") {
            unlink('uploads/portfolio/big/' . $image);
            unlink('uploads/portfolio/small/' . $image);
        }

        if ($portfolio->fill($input)->save()) {
            return response()->json([
                'success' => true,
                'message' => 'User Added Successfully.',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
