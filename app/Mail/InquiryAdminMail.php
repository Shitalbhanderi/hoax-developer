<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Arr;

class InquiryAdminMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
          $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view=$this->view('front.contactus.Adminmail');
        if(Arr::has($this->user,['attachment'])){
            $view=$view->attach($this->user['attachment_file']['path'], [
            'as' => $this->user['attachment_file']['name'],
            'mime' => $this->user['attachment_file']['mimetype'],
        ]);
        }
        return $view
            ->with([
                'user' => $this->user,
            ])
            ->subject('HoaxDeveloper - ContactUs -'.$this->user['email']);
    }
}
