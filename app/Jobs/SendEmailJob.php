<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\InquiryAdminMail as InquiryAdminMailtest;
use App\Mail\InquiryUserMail as InquirysUserMailtest;
use Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user=$this->user;
        // dd($user);
        $adminemail = new InquiryAdminMailtest($this->user);
        $useremail = new InquiryAdminMailtest($this->user);
        Mail::to('shital.9brainz@gmail.com')->send($useremail,$user);
        Mail::to($this->user['email'])->send($adminemail,$user);
       

        // Mail::send('front.contactus.Adminmail',['user' => $this->user], function($message) 
        //  {
        //     $message->to('shital.9brainz@gmail.com', 'Admin')->subject('Contact Us - Inquiery');
        // });

    }
}
