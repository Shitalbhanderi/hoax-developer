<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMediaIcon extends Model
{
    protected $fillable = ['icon', 'url', 'contact_us_id'];

    protected $append = ['icon_url'];

    public function getIconUrlAttribute()
    {
        return ($this->attributes['icon'] && file_exists('uploads/socialmedia/' . $this->attributes['icon'])) ? asset('/uploads/socialmedia/' . $this->attributes['icon']) : asset('/defaultimage/socialmedia-defualt.png');
    }
    public function getUrlAttribute()
    {
        $str = $this->attributes['url'];
        if ($str != null) {
            $substr = substr($str, 0, 4);
            if ($substr == 'http') {
                return $str;
            } else {

                return $this->attributes['url'] = 'http://' . $str;
            }
        }
    }
}
