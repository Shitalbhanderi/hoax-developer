<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
  protected $fillable = [
    'section_header',
    'section_title',
    'portfolio_item_andriod_url',
    'portfolio_item_ios_url',
    'portfolio_item_image',
    'portfolio_item_name',
    'portfolio_item_andriod',
    'portfolio_item_ios',
    'portfolio_item_name_color'
  ];
  protected $append = ['portfolio_item_image_big_url', 'portfolio_item_image_small_url'];

  public function setSectionHeaderAttribute($value)
  {
    $this->attributes['section_header'] = strtoupper($value);
  }
  public function setSectionTitleAttribute($value)
  {
    $this->attributes['section_title'] = ucfirst($value);
  }
  public function getPortfolioItemImageBigUrlAttribute()
  {
    return ($this->attributes['portfolio_item_image'] && file_exists('uploads/portfolio/big/' . $this->attributes['portfolio_item_image'])) ? asset('/uploads/portfolio/big/' . $this->attributes['portfolio_item_image']) : asset('/defaultimage/portfolio-big-defualt.jpg');
  }
  public function getPortfolioItemImageSmallUrlAttribute()
  {
    return ($this->attributes['portfolio_item_image'] && file_exists('uploads/portfolio/small/' . $this->attributes['portfolio_item_image'])) ? asset('/uploads/portfolio/small/' . $this->attributes['portfolio_item_image']) : asset('/defaultimage/portfolio-small-defualt.jpg');
  }
  public function getPortfolioItemIosUrlAttribute()
  {
      $str=$this->attributes['portfolio_item_ios_url'];
      if($str!=null){
        $substr=substr($str,0,4);
        if ($substr == 'http'){
            return $str;
        }else{
            return $this->attributes['portfolio_item_ios_url']='http://'.$str;
        }  
      }
  }
  public function getPortfolioItemAndriodUrlAttribute()
  {
    $str=$this->attributes['portfolio_item_andriod_url'];
    if($str!=null){
      $substr=substr($str,0,4);
      if ($substr == 'http'){
          return $str;
      }else{
          return $this->attributes['portfolio_item_andriod_url']='http://'.$str;
      }  
    }
 }
}
