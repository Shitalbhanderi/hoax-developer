<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Home extends Model
{
    protected $fillable = [
        'head_title',
        'hero_description',
        'header_button',
        'header_button_link',
        'intro_img'
    ];
    protected $append = ['header_button_url', 'intro_img_url'];

    public function getHeaderButtonUrlAttribute()
    {
        return ($this->attributes['header_button'] && file_exists('uploads/home/' . $this->attributes['header_button'])) ? asset('/uploads/home/' . $this->attributes['header_button']) : asset('/defaultimage/googleplay-defualt.png');
    }
    public function getIntroImgUrlAttribute()
    {
        return ($this->attributes['intro_img'] && file_exists('uploads/home/' . $this->attributes['intro_img'])) ? asset('/uploads/home/' . $this->attributes['intro_img']) : asset('/defaultimage/intro-defualt.svg');
    }
    public function getHeaderButtonLinkAttribute()
    {
        $str=$this->attributes['header_button_link'];
        if($str!=null){
          $substr=substr($str,0,4);
          if ($substr == 'http'){
              return $str;
          }else{
              return $this->attributes['header_button_link']='http://'.$str;
          }  
        }
    }
}
