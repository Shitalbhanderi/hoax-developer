<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
  protected $fillable = [
    'section_header',
    'section_title',
    'service_item_icon',
    'service_item_content_heading',
    'service_item_content_description'
  ];
  protected $append = ['service_item_icon_url', 'icon_bg_url'];

  public function setSectionHeaderAttribute($value)
  {
    $this->attributes['section_header'] = strtoupper($value);
  }
  public function setSectionTitleAttribute($value)
  {
    $this->attributes['section_title'] = ucfirst($value);
  }
  public function getServiceItemIconUrlAttribute()
  {
    return ($this->attributes['service_item_icon'] && file_exists('uploads/services/' . $this->attributes['service_item_icon'])) ? asset('/uploads/services/' . $this->attributes['service_item_icon']) : asset('/defaultimage/service-defualt.png');
  }
}
