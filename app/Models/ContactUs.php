<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $fillable = [
        'logo', 'footer_text', 'skype_id'
    ];

    protected $append = ['logo_url'];

    public function getLogoUrlAttribute()
    {
        return ($this->attributes['logo'] && file_exists('uploads/contactus/' . $this->attributes['logo'])) ? asset('/uploads/contactus/' . $this->attributes['logo']) : asset('/defaultimage/contactus-defualt.png');
    }
}
