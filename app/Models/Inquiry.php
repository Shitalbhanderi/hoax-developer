<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    // public $table='Inquiries';
    protected $fillable = ['fullname', 'email', 'skype_id', 'country', 'project_type', 'budget', 'attachment', 'description'
    ];
}
