<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $fillable = [
		'section_header',
		'section_title',
		'blog_item_image',
		'blog_item_content_heading',
		'blog_item_content_description',
		'auther'
	];
	protected $append = ['blog_item_big_image_url', 'blog_item_medium_image_url', 'blog_item_small_image_url'];

	public function setSectionHeaderAttribute($value)
	{
		$this->attributes['section_header'] = strtoupper($value);
	}
	public function getBlogItemContentHeadingAttribute($value)
    {
        return ucwords($value);
    }
    public function getAutherAttribute($value)
    {
        return ucwords($value);
    }

	public function setSectionTitleAttribute($value)
	{
		$this->attributes['section_title'] = ucfirst($value);
	}
	public function getBlogItemBigImageUrlAttribute()
	{
		return ($this->attributes['blog_item_image'] && file_exists('uploads/blogs/blog-big/' . $this->attributes['blog_item_image'])) ? asset('/uploads/blogs/blog-big/' . $this->attributes['blog_item_image']) : asset('/defaultimage/blog-big-defualt.jpg');
	}
	public function getBlogItemMediumImageUrlAttribute()
	{
		return ($this->attributes['blog_item_image'] && file_exists('uploads/blogs/blog-medium/' . $this->attributes['blog_item_image'])) ? asset('/uploads/blogs/blog-medium/' . $this->attributes['blog_item_image']) : asset('/defaultimage/blog-medium-defualt.jpg');
	}
	public function getBlogItemSmallImageUrlAttribute()
	{
		
		return ($this->attributes['blog_item_image'] && file_exists('uploads/blogs/blog-small/' . $this->attributes['blog_item_image'])) ? asset('/uploads/blogs/blog-small/' . $this->attributes['blog_item_image']) : asset('/defaultimage/blog-small-defualt.jpg');
	}
}
