<?php

namespace App\Domain\Admin\Datatables;

use App\Domain\Util\Datatables\BaseDatatableScope;
use App\Models\Inquiry;

class InquiryDatatableScope extends BaseDatatableScope
{
    /**
     * AppDatatableScope constructor.
     */
    public function __construct()
    {
        $this->setHtml([
            [
                'data' => 'fullname',
                'name' => 'fullname',
                'title' => 'FullName',
            ],
            [
                'data' => 'email',
                'name' => 'email',
                'title' => 'Email',
            ],
            [
                'data' => 'skype_id',
                'name' => 'skype_id',
                'title' => 'Skype Id',
            ],
            [
                'data' => 'country',
                'name' => 'country',
                'title' => 'Country',
            ],
            [
                'data' => 'project_type',
                'name' => 'project_type',
                'title' => 'Project Type',
            ],
            [
                'data' => 'budget',
                'name' => 'budget',
                'title' => 'Budget',
            ],
            [
                'data' => 'attachment',
                'name' => 'attachment',
                'title' => 'Attachment',
            ],
            [
                'data' => 'description',
                'name' => 'description',
                'title' => 'Description',
            ],
        ]);
        
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function query()
    {
        return datatables()->eloquent(Inquiry::query())->make(true);
    }
}
