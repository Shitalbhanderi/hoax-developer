<?php

namespace App\Domain\Admin\Request;

use App\Domain\Util\ImageUtil;
use Illuminate\Foundation\Http\FormRequest;
use App\Domain\Util\ImageUpload;


class ServicesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'service_item_icon.dimensions' => 'Image resolution less than 500*500',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'section_header' => 'required|min:3|max:30',
            'section_title' => 'required|min:3|max:60',
            'service_item_icon' => 'image|dimensions:max_width=500,max_height=500',
            'service_item_content_heading' => 'required|min:3|max:30',
            'service_item_content_description' => 'required|min:3|max:130',
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only(
                'section_header',
                'section_title',
                'service_item_content_heading',
                'service_item_content_description'
            ),
            $this->hasFile('service_item_icon') ? ['service_item_icon' => $this->uploadImage('service_item_icon')] : []

        );
    }
    protected function uploadImage($image)
    {
        $originalImage = $this->file($image);
        $path = public_path() . '/uploads/services';
        $imgname = '_services_';
        return (new ImageUpload())->uploadImageUtil($originalImage, $path, 50, 50, $imgname);
    }
}
