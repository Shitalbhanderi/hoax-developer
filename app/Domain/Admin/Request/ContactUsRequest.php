<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\Domain\Util\ImageUpload;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'logo.dimensions' => 'Image resolution less than 1000*1000',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'logo' => 'image|dimensions:max_width=1000,max_height=1000',
            'footer_text' => 'required|max:230',
            'skype_id' => 'required|max:50',
        ];
    }
    public function persist()
    {
        return array_merge(
            $this->only(
                'skype_id',
                'footer_text'
            ),
            $this->hasFile('logo') ? ['logo' => $this->uploadImage('logo')] : []
        );
    }
    protected function uploadImage($image)
    {
        $originalImage = $this->file($image);
        $path = public_path() . '/uploads/contactus';
        $imgname = '_logo_';
        return (new ImageUpload())->uploadImageUtil($originalImage, $path, 160, 55, $imgname);
    }
}
