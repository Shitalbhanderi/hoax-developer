<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;
// use App\Domain\Util\ImageUpload;
use File;
use Image;

class BlogsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'blog_item_image.dimensions' => 'Image resolution less than 4000*4000',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->all());
        return [
            'section_header' => 'required|min:3|max:30',
            'section_title' => 'required|min:3|max:60',
            'blog_item_image' => 'image|dimensions:max_width=4000,max_height=4000',
            'blog_item_content_heading' => 'required|min:3|max:70',
            'blog_item_content_description' => 'required|min:3',
            'auther'=>'required|max:20',
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only(
                'section_header',
                'section_title',
                'blog_item_content_heading',
                'blog_item_content_description',
                'auther'
            ),
            $this->hasFile('blog_item_image') ? ['blog_item_image' => $this->uploadImage('blog_item_image')] : []
        );
    }
    protected function uploadImage($image)
    {
        $blogmedium = public_path() . '/uploads/blogs/blog-medium';
        $blogbig = public_path() . '/uploads/blogs/blog-big';
        $blogsmall = public_path() . '/uploads/blogs/blog-small';
        File::exists($blogmedium) or mkdir($blogmedium, 0777, true);
        File::exists($blogbig) or mkdir($blogbig, 0777, true);
        File::exists($blogsmall) or mkdir($blogsmall, 0777, true);

        $originalImage = $this->file($image);
        $image = Image::make($originalImage->getRealPath());
        $imgname = '_blog_';
        $imageName = uniqid() . $imgname . time() . '.' . $originalImage->getClientOriginalExtension();
        $image->fit(555, 653, function ($constraint) {
            $constraint->aspectRatio();
        })->save($blogmedium . '/' . $imageName);
        $image->fit(1140, 450, function ($constraint) {
            $constraint->aspectRatio();
        })->save($blogbig . '/' . $imageName);
        $image->fit(350, 250, function ($constraint) {
            $constraint->aspectRatio();
        })->save($blogsmall . '/' . $imageName);

        return $imageName;
    }
}
