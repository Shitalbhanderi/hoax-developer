<?php

namespace App\Domain\Admin\Request;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Image;
use File;

class PortfolioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'portfolio_item_image.dimensions' => 'Image resolution less than 4000*4000',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'section_header' => 'required|min:3|max:30',
            'section_title' => 'required|min:3|max:60',
            'portfolio_item_name' => 'required|max:30',
            'portfolio_item_image' => 'image|dimensions:max_width=4000,max_height=4000',
            'portfolio_item_ios_url' => Rule::requiredIf($this->has('ios')),
            'portfolio_item_andriod_url' => Rule::requiredIf($this->has('android')),
            'portfolio_item_ios_url' => ($this->has('ios'))?['regex:/^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/']:[],
            'portfolio_item_android_url' => ($this->has('android'))?['regex:/^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/']:[],
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only(
                'section_header',
                'section_title',
                'portfolio_item_name',
                'portfolio_item_name_color'
            ),
           ['portfolio_item_andriod_url' => $this->has('android') ? $this->portfolio_item_andriod_url: null],
           ['portfolio_item_ios_url' => $this->has('ios') ? $this->portfolio_item_ios_url: null],

            $this->hasFile('portfolio_item_image') ? ['portfolio_item_image' => $this->uploadImage('portfolio_item_image')] : [],
            [
                'portfolio_item_andriod' => $this->has('android') ? 1 : 0,
                'portfolio_item_ios' => $this->has('ios') ? 1 : 0,
            ]
        );
    }

    protected function uploadImage($image)
    {

        $originalImage = $this->file($image);

        $originalPath = $originalImage->getRealPath();

        $image = Image::make($originalPath);

        $path_big = public_path() . '/uploads/portfolio/big';
        $path_small = public_path() . '/uploads/portfolio/small';
        File::exists($path_big) or mkdir($path_big, 0777, true);
        File::exists($path_small) or mkdir($path_small, 0777, true);
        $imageName = uniqid() . '_portfolio_' . time() . '.' . $originalImage->getClientOriginalExtension();

        $image->fit(532, 308, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path_big . '/' . $imageName);

        $image->fit(251, 308, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path_small . '/' . $imageName);


        return $imageName;
    }
}
