<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\Domain\Util\ImageUpload;
use File;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'introl_img.dimensions' => 'Image resolution less than 4000*4000',
            'header_button.dimensions' => 'Image resolution less than 1000*1000',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $regex = '/^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)/?$/';
        return [
            'head_title' => 'required|min:3|max:35',
            'hero_description' => 'required|min:3|max:250',
            'header_button_link' => ['required','max:300','regex:/^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/'],
            'introl_img' => 'image|svg|dimensions:max_width=4000,max_height=4000',
            'header_button' => 'image|dimensions:max_width=1000,max_height=1000',
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only(
                'head_title',
                'hero_description',
                'header_button_link'
            ),
            $this->hasFile('header_button') ? ['header_button' => $this->uploadImage('header_button')] : [],

            $this->hasFile('intro_img') ? ['intro_img' => $this->uploadImage2('intro_img')] : []
        );
    }
    protected function uploadImage($image)
    {
        $originalImage = $this->file($image);
        $path = public_path() . '/uploads/home';
        $imgname = '_home_';
        return (new ImageUpload())->uploadImageUtil($originalImage, $path, 200, 60, $imgname);
    }
    public function uploadImage2($header_button)
    {
        $path = public_path() . '/uploads/home';
        $imageName = time() . '.' . $this->file($header_button)->getClientOriginalExtension();
        File::exists($path) or mkdir($path, 0777, true);
        $this->file($header_button)->move($path, $imageName);
        return $imageName;
    }
}
