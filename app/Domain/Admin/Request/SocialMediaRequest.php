<?php

namespace App\Domain\Admin\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use App\Domain\Util\ImageUpload;


class SocialMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'socialmedia.*.logo.dimensions' => 'Image resolution less than 500*500',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'socialmedia.*.logo' => 'image|dimensions:max_width=500,max_height=500',
            'socialmedia.*.link' => ['required','max:300','regex:/^http:\/\/|(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/'],
        ];
    }
    public function persist()
    {
        $arr[] = array();
        $i = 0;
        if($this->socialmedia){
            foreach ($this->socialmedia as $key => $value) {
                $arr[$i]['url'] = $value['link'];
                Arr::has($value, 'logo') ? $arr[$i]['icon'] = $this->uploadImage($value['logo']) : [];
                $arr[$i]['contact_us_id'] = $this->hidden;
                $arr[$i]['id'] = $value['id'];
                $i++;
            }  
        }
        return $arr;
    }
    protected function uploadImage($image)
    {
        $originalImage = $image;
        $path = public_path() . '/uploads/socialmedia';
        $imgname = '_socialmedia_';
        return (new ImageUpload())->uploadImageUtil($originalImage, $path, 30, 30, $imgname);
    }
}
