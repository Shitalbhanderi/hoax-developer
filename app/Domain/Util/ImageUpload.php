<?php

namespace App\Domain\Util;

use Image;
use File;

class ImageUpload
{
    public function uploadImageUtil($originalImage, $path, $w, $h, $imgname)
    {
        $image = Image::make($originalImage->getRealPath());
        File::exists($path) or mkdir($path, 0777, true);
        $imageName = uniqid() . $imgname . time() . '.' . $originalImage->getClientOriginalExtension();
        $image->fit($w, $h, function ($constraint) {
            $constraint->aspectRatio();
        })->save($path . '/' . $imageName);
        return $imageName;
    }
}
