<?php

namespace App\Domain\Front\Request;

use Illuminate\Foundation\Http\FormRequest;
use File;

class InquiryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'email' => 'required_without:skype_id|email',
            'skype_id' => 'required_without:email|max:50',
            'description' => 'required',
            'attachment' => 'max:25000',
        ];
    }

    public function persist()
    {
        return array_merge(
            $this->only(
                'fullname',
                'email',
                'skype_id',
                'country',
                'project_type',
                'budget',
                'description'
            ),
            $this->hasFile('attachment') ? ['attachment' => $this->attachment->getClientOriginalName()] : [],
            $this->hasFile('attachment') ? ['attachment_file' => $this->uploadFile($this->attachment)] : []
        );
    }

    protected function uploadFile($file)
    {
        $path = $file->getRealPath();
        $imageName = time().'.'.$file->getClientOriginalExtension();
        $path = public_path().'/uploads/mail';
        File::exists($path) or mkdir($path, 0777, true);
        $file->move($path, $imageName);

        return [
            'path' => $path.'/'.$imageName,
            'name' => $file->getClientOriginalName(),
            'mimetype' => $file->getClientMimeType(),
        ];
    }
}
