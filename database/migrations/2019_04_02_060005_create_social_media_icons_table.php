<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialMediaIconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('social_media_icons')) {
        Schema::create('social_media_icons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contact_us_id');
            $table->foreign('contact_us_id')->references('id')->on('contact_us');
            $table->string('icon')->nullable();
            $table->text('url');
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_media_icons');
    }
}
