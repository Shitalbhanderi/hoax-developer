<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('section_header');
            $table->string('section_title');
            $table->string('portfolio_item_image');
            $table->string('portfolio_item_name');
            $table->boolean('portfolio_item_andriod')->default(false);
            $table->boolean('portfolio_item_ios')->default(false);
            $table->text('portfolio_item_andriod_url')->nullable();
            $table->text('portfolio_item_ios_url')->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
