<?php

use Illuminate\Database\Seeder;
use App\Models\Home;

class HomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Home::create([
	            'head_title' => str_random(8),
	            'hero_description' => str_random(12),
	            'header_button_link' => str_random(12),
	            'header_button' => str_random(6).'jpg',
	            'intro_img' => str_random(6).'jpg'
	    ]);
    }
}
