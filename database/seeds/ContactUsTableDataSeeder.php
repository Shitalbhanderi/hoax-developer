<?php

use Illuminate\Database\Seeder;
use App\Models\ContactUs;

class ContactUsTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactUs::create([
	            'logo' => str_random(8).'jpg',
	            'footer_text' => str_random(12),
	            'skype_id' => str_random(12)
	    ]);
    }
}
