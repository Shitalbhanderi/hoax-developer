<?php

use Illuminate\Database\Seeder;
use App\Models\Services;

class ServicesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i=1;$i<=6;$i++){
	        Services::create([
	            'section_header' => str_random(8),
	            'section_title' => str_random(12),
	            'service_item_icon' => str_random(12).'jpg',
	            'service_item_content_heading' => str_random(12),
	            'service_item_content_description' => str_random(12)
		    ]);
	    }
    }
}
