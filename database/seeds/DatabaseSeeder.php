<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ]);
      

        $this->call(HomeTableSeeder::class);
        $this->call(ContactUsTableDataSeeder::class);
        $this->call(BlogTableDataSeeder::class);
        $this->call(PortfolioTableDataSeeder::class);
        $this->call(ServicesTableDataSeeder::class);
        $this->call(SocialMediaIconTableDataSeeder::class);
        
    }
}
