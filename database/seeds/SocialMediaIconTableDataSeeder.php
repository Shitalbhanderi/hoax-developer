<?php

use Illuminate\Database\Seeder;
use App\Models\SocialMediaIcon;

class SocialMediaIconTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SocialMediaIcon::create([
            'icon' => str_random(8),
            'url' => str_random(12),
            'contact_us_id' => 1
	    ]);
    }
}
