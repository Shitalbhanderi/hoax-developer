<?php

use Illuminate\Database\Seeder;
use App\Models\Portfolio;

class PortfolioTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i=1;$i<=6;$i++){
	        Portfolio::create([
	            'section_header' => str_random(8),
	            'section_title' => str_random(12),
	            'portfolio_item_image' => str_random(12).'jpg',
	            'portfolio_item_name' => str_random(12)
		    ]);
     	}
    }
}
