<?php

use Illuminate\Database\Seeder;
use App\Models\Blog;


class BlogTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=2;$i++){
            Blog::create([
                'section_header' => str_random(8),
                'section_title' => str_random(12),
                'blog_item_image' => str_random(12).'jpg',
                'blog_item_content_heading' => str_random(12),
                'blog_item_content_description' => str_random(1200),
                'auther' => str_random(12)
    	    ]);
        }
         
    }
}
