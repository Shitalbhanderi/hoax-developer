<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="143.631" height="143.631" viewBox="0 0 143.631 143.631">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.735" y2="0.895" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#efebff"/>
      <stop offset="1" stop-color="#ecfbff"/>
    </linearGradient>
    <linearGradient id="linear-gradient-3" x1="0.126" y1="0.023" x2="1.172" y2="1.301" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#9cb3ff"/>
      <stop offset="1" stop-color="#b0ffe9"/>
    </linearGradient>
  </defs>
  <g id="Group_290" data-name="Group 290" transform="translate(-885 -1318)">
    <ellipse id="Ellipse_5" data-name="Ellipse 5" cx="71.815" cy="71.815" rx="71.815" ry="71.815" transform="translate(885 1318)" fill="url(#linear-gradient)"/>
    <ellipse id="uiObjectBg" cx="43.876" cy="43.876" rx="43.876" ry="43.876" transform="translate(913 1346)" fill="url(#linear-gradient-3)"/>
   <!--  <path id="Path_413" data-name="Path 413" d="M14.244-3.187A19.993,19.993,0,0,1,11.405.448,4.919,4.919,0,0,1,7.819,1.992a6.909,6.909,0,0,1-3.387-.747A7.505,7.505,0,0,0,.8.5a7.81,7.81,0,0,0-3.686.747,7.746,7.746,0,0,1-3.287.847A5.148,5.148,0,0,1-9.911.448,20.556,20.556,0,0,1-12.85-3.287,27.011,27.011,0,0,1-16.934-13.5,16.993,16.993,0,0,1-15.539-24.7a11.937,11.937,0,0,1,3.785-3.835,9.162,9.162,0,0,1,4.98-1.444,10.735,10.735,0,0,1,3.885.9,9.7,9.7,0,0,0,3.088.9,11.276,11.276,0,0,0,3.437-1.046,10.552,10.552,0,0,1,4.731-.847,12.337,12.337,0,0,1,3.387.747A9,9,0,0,1,16.037-25.9,11.047,11.047,0,0,0,13.7-23.807a9.293,9.293,0,0,0-2.241,5.977q.4,4.781,2.889,6.724a11.468,11.468,0,0,0,2.889,1.942q-.1.2-.8,1.942A23.865,23.865,0,0,1,14.244-3.187ZM2.092-37.154A8.988,8.988,0,0,1,5.03-39.4a8.935,8.935,0,0,1,3.337-.946,7.922,7.922,0,0,1-.448,3.636,11.548,11.548,0,0,1-1.743,3.138,8.085,8.085,0,0,1-2.789,2.241A6.835,6.835,0,0,1-.1-30.58a7.525,7.525,0,0,1,.5-3.636A10.843,10.843,0,0,1,2.092-37.154Z" transform="translate(957.311 1409.342)" fill="#fff"/> -->
  </g>
      <circle class="circle2_dot1" id="Ellipse_17" data-name="Ellipse 17" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5" transform="translate(50 59.431)"/>
      <circle class="circle2_dot2" id="Ellipse_18" data-name="Ellipse 18" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5" transform="translate(60 59.431)"/>
      <circle class="circle2_dot3" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.7" transform="translate(70 59.431)"/>
      <circle class="circle2_dot4" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="transparent" fill="transparent" stroke-width="1.7"  transform="translate(55 55)"/>

</svg>













<!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="148.019" height="148.019" viewBox="0 0 148.019 148.019">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.735" y2="0.895" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#efebff"/>
      <stop offset="1" stop-color="#ecfbff"/>
    </linearGradient>
   
    <linearGradient id="linear-gradient-3" x1="0.126" y1="0.023" x2="1.172" y2="1.301">
      <stop offset="0" stop-color="#F66A00"/>
      <stop offset="1" stop-color="#FFFFFF"/>
    </linearGradient>
  </defs>
  <g id="Group_271" data-name="Group 271" transform="translate(646 -1268)">
    <ellipse id="Ellipse_5" data-name="Ellipse 5" cx="74.009" cy="74.009" rx="74.009" ry="74.009" transform="translate(-646 1268)" fill="url(#linear-gradient)"/>
    <ellipse id="uiObjectBg" cx="45.216" cy="45.216" rx="45.216" ry="45.216" transform="translate(-617 1296)" fill="url(#linear-gradient-3)"/>
    <g id="Group_205" data-name="Group 205" transform="translate(-642.383 1282.931)">
     
    </g>
    <path id="Path_413" data-name="Path 413" d="M14.244-3.187A19.993,19.993,0,0,1,11.405.448,4.919,4.919,0,0,1,7.819,1.992a6.909,6.909,0,0,1-3.387-.747A7.505,7.505,0,0,0,.8.5a7.81,7.81,0,0,0-3.686.747,7.746,7.746,0,0,1-3.287.847A5.148,5.148,0,0,1-9.911.448,20.556,20.556,0,0,1-12.85-3.287,27.011,27.011,0,0,1-16.934-13.5,16.993,16.993,0,0,1-15.539-24.7a11.937,11.937,0,0,1,3.785-3.835,9.162,9.162,0,0,1,4.98-1.444,10.735,10.735,0,0,1,3.885.9,9.7,9.7,0,0,0,3.088.9,11.276,11.276,0,0,0,3.437-1.046,10.552,10.552,0,0,1,4.731-.847,12.337,12.337,0,0,1,3.387.747A9,9,0,0,1,16.037-25.9,11.047,11.047,0,0,0,13.7-23.807a9.293,9.293,0,0,0-2.241,5.977q.4,4.781,2.889,6.724a11.468,11.468,0,0,0,2.889,1.942q-.1.2-.8,1.942A23.865,23.865,0,0,1,14.244-3.187ZM2.092-37.154A8.988,8.988,0,0,1,5.03-39.4a8.935,8.935,0,0,1,3.337-.946,7.922,7.922,0,0,1-.448,3.636,11.548,11.548,0,0,1-1.743,3.138,8.085,8.085,0,0,1-2.789,2.241A6.835,6.835,0,0,1-.1-30.58a7.525,7.525,0,0,1,.5-3.636A10.843,10.843,0,0,1,2.092-37.154Z" transform="translate(-571.689 1360.342)" fill="#fff"/>
  </g>

      <circle class="circle2_dot1" id="Ellipse_17" data-name="Ellipse 17" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(50 59.431)"/>

      <circle class="circle2_dot2" id="Ellipse_18" data-name="Ellipse 18" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(60 59.431)"/>

      <circle class="circle2_dot3" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.7"  transform="translate(70 59.431)"/>

       <circle class="circle2_dot4" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="transparent" fill="transparent" stroke-width="1.7"  transform="translate(55 55)"/>
</svg>
 -->