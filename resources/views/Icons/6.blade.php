<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fde4ff"/>
      <stop offset="1" stop-color="#fffdf9"/>
    </linearGradient>
    <linearGradient id="linear-gradient-5" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ffa8e8"/>
      <stop offset="1" stop-color="#ffe6e6"/>
    </linearGradient>
  </defs>
  <g id="Group_311" data-name="Group 311" transform="translate(-503 -1917)">
    <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(503 1917)" fill="url(#linear-gradient)"/>
    <ellipse id="feObjectBg" cx="43.509" cy="43.509" rx="43.509" ry="43.509" transform="translate(530.846 1944.597)" fill="url(#linear-gradient-5)"/>
  </g>
    <polygon class="circle4_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" />
    <polygon class="circle4_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle4_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle4_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
</svg>








