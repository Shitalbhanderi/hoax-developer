<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fedcdb"/>
      <stop offset="1" stop-color="#fff6f1"/>
    </linearGradient>
    <linearGradient id="linear-gradient-7" x2="0.802" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ff8989"/>
      <stop offset="1" stop-color="#ffddc5"/>
    </linearGradient>
  </defs>
  <g id="Group_313" data-name="Group 313" transform="translate(-1262 -1918)">
    <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(1262 1918)" fill="url(#linear-gradient)"/>
    <path id="brandObjectBg" d="M43.511,0A43.511,43.511,0,1,1,0,43.511,43.511,43.511,0,0,1,43.511,0Z" transform="translate(1290.14 1946.14)" fill="url(#linear-gradient-7)"/>
  
  </g>
    <polygon class="circle6_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" />
    <polygon class="circle6_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle6_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle6_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
</svg>

