<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="140.702" height="140.702" viewBox="0 0 140.702 140.702">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.776" y2="0.906" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ecdfff"/>
      <stop offset="1" stop-color="#eaf9ff"/>
    </linearGradient>
    <linearGradient id="linear-gradient-4" x1="0.837" y1="1" x2="-0.205" y2="-0.141" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#e7d1ff"/>
      <stop offset="1" stop-color="#8383ef"/>
    </linearGradient>
  </defs>
  <g id="Group_310" data-name="Group 310" transform="translate(-1264 -1357)">
    <ellipse id="Ellipse_20" data-name="Ellipse 20" cx="70.351" cy="70.351" rx="70.351" ry="70.351" transform="translate(1264 1357)" fill="url(#linear-gradient)"/>
    <ellipse id="illusObjectBg" cx="42.981" cy="42.981" rx="42.981" ry="42.981" transform="translate(1291 1385)" fill="url(#linear-gradient-4)"/>
   
  </g>
    <polygon class="circle3_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />
    <polygon class="circle3_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle3_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle3_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>

</svg>


