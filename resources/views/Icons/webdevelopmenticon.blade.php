<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fde4ff"/>
      <stop offset="1" stop-color="#fffdf9"/>
    </linearGradient>
    <linearGradient id="linear-gradient-5" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ffa8e8"/>
      <stop offset="1" stop-color="#ffe6e6"/>
    </linearGradient>
  </defs>
  <g id="Group_311" data-name="Group 311" transform="translate(-503 -1917)">
    <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(503 1917)" fill="url(#linear-gradient)"/>
    <ellipse id="feObjectBg" cx="43.509" cy="43.509" rx="43.509" ry="43.509" transform="translate(530.846 1944.597)" fill="url(#linear-gradient-5)"/>
   <!--  <path id="Path_416" data-name="Path 416" d="M-19.252-40.342a3.934,3.934,0,0,0-2.9,1.16,4.29,4.29,0,0,0-1.257,2.9v24.744a4.4,4.4,0,0,0,1.257,3,3.934,3.934,0,0,0,2.9,1.16h14.4v4.156H-8.91V.834H7.522v-4.06H3.462V-7.382h14.4a3.854,3.854,0,0,0,2.9-1.208,4.475,4.475,0,0,0,1.257-2.948V-36.282a4.29,4.29,0,0,0-1.257-2.9,3.934,3.934,0,0,0-2.9-1.16Zm0,4.06H17.864v24.744H-19.252ZM5.492-34.156-1.757-27l7.249,7.249,2.9-2.9L4.042-27l4.35-4.253ZM-6.88-27.97l-2.9,2.9,4.35,4.253-4.35,4.35,2.9,2.9L.369-20.817Z" transform="translate(575.408 2008.342)" fill="#fff"/> -->
  </g>
    <polygon class="circle4_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" />
    <polygon class="circle4_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle4_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle4_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
</svg>











<!-- 

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="146.779" height="146.779" viewBox="0 0 146.779 146.779">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fde4ff"/>
      <stop offset="1" stop-color="#fffdf9"/>
    </linearGradient>
  
     <linearGradient id="linear-gradient-5" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#25B043"/>
      <stop offset="1" stop-color="#B6F0C2"/>
    </linearGradient>
  </defs>
  <g id="Group_273" data-name="Group 273" transform="translate(552 -1696)">
    <g id="Group_272" data-name="Group 272" transform="translate(-260 630)">
      <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="73.39" cy="73.39" rx="73.39" ry="73.39" transform="translate(-292 1066)" fill="url(#linear-gradient)"/>
      <ellipse id="feObjectBg" cx="44.838" cy="44.838" rx="44.838" ry="44.838" transform="translate(-263.304 1094.44)" fill="url(#linear-gradient-5)"/>
      <g id="Group_206" data-name="Group 206" transform="translate(-288.413 1080.931)">
       
      </g>
    </g>
    <path id="Path_440" data-name="Path 440" d="M-19.125-40.342a4.054,4.054,0,0,0-2.988,1.2,4.421,4.421,0,0,0-1.295,2.988v25.5A4.535,4.535,0,0,0-22.113-7.57a4.054,4.054,0,0,0,2.988,1.2H-4.283v4.283H-8.467V2.092H8.467V-2.092H4.283V-6.375H19.125A3.971,3.971,0,0,0,22.113-7.62a4.612,4.612,0,0,0,1.295-3.038v-25.5a4.421,4.421,0,0,0-1.295-2.988,4.054,4.054,0,0,0-2.988-1.2Zm0,4.184h38.25v25.5h-38.25Zm25.5,2.191L-1.1-26.6l7.471,7.471,2.988-2.988L4.881-26.6l4.482-4.383Zm-12.75,6.375L-9.363-24.6l4.482,4.383-4.482,4.482,2.988,2.988L1.1-20.221Z" transform="translate(-478.592 1788.342)" fill="#fff"/>
  </g>
      

      <polygon class="circle4_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />

    <polygon class="circle4_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>

    <polygon class="circle4_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle4_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
</svg>
 -->