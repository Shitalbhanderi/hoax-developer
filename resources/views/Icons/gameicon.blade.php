<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="140.702" height="140.702" viewBox="0 0 140.702 140.702">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.776" y2="0.906" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ecdfff"/>
      <stop offset="1" stop-color="#eaf9ff"/>
    </linearGradient>
    <linearGradient id="linear-gradient-4" x1="0.837" y1="1" x2="-0.205" y2="-0.141" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#e7d1ff"/>
      <stop offset="1" stop-color="#8383ef"/>
    </linearGradient>
  </defs>
  <g id="Group_310" data-name="Group 310" transform="translate(-1264 -1357)">
    <ellipse id="Ellipse_20" data-name="Ellipse 20" cx="70.351" cy="70.351" rx="70.351" ry="70.351" transform="translate(1264 1357)" fill="url(#linear-gradient)"/>
    <ellipse id="illusObjectBg" cx="42.981" cy="42.981" rx="42.981" ry="42.981" transform="translate(1291 1385)" fill="url(#linear-gradient-4)"/>
   <!--  <path id="Path_417" data-name="Path 417" d="M-10.658-31.875H10.658a12.42,12.42,0,0,1,8.965,3.735,13.058,13.058,0,0,1,3.785,9.015,13.058,13.058,0,0,1-3.785,9.015,12.42,12.42,0,0,1-8.965,3.735A12.384,12.384,0,0,1,5.329-7.521,13.16,13.16,0,0,1,1.1-10.658H-1.1A13.16,13.16,0,0,1-5.329-7.521a12.384,12.384,0,0,1-5.329,1.146,12.42,12.42,0,0,1-8.965-3.735,13.058,13.058,0,0,1-3.785-9.015,13.058,13.058,0,0,1,3.785-9.015A12.42,12.42,0,0,1-10.658-31.875ZM-12.75-25.5v4.283h-4.283v4.184h4.283v4.283h4.283v-4.283h4.184v-4.184H-8.467V-25.5ZM7.471-19.125a3.1,3.1,0,0,0-2.291.9,3.1,3.1,0,0,0-.9,2.291,3.1,3.1,0,0,0,.9,2.291,3.041,3.041,0,0,0,2.241.9,3.041,3.041,0,0,0,2.241-.9,3.1,3.1,0,0,0,.9-2.291,3.1,3.1,0,0,0-.9-2.291A2.981,2.981,0,0,0,7.471-19.125ZM13.846-25.5a3.1,3.1,0,0,0-2.291.9,3.1,3.1,0,0,0-.9,2.291,3.1,3.1,0,0,0,.9,2.291,3.041,3.041,0,0,0,2.241.9,3.041,3.041,0,0,0,2.241-.9,3.1,3.1,0,0,0,.9-2.291,3.1,3.1,0,0,0-.9-2.291A2.981,2.981,0,0,0,13.846-25.5Z" transform="translate(1334.408 1446.875)" fill="#fff"/> -->
  </g>
    <polygon class="circle3_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />
    <polygon class="circle3_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle3_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle3_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>

</svg>





<!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="145" height="145" viewBox="0 0 145 145">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.776" y2="0.906" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ecdfff"/>
      <stop offset="1" stop-color="#eaf9ff"/>
    </linearGradient>
  
     <linearGradient id="linear-gradient-4" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#EE52E2"/>
     
      <stop offset="1" stop-color="#E7CBE5"/>
    </linearGradient>
  </defs>
  <g id="Group_271" data-name="Group 271" transform="translate(827 -1479)">
    <ellipse id="Ellipse_24" data-name="Ellipse 24" cx="72.5" cy="72.5" rx="72.5" ry="72.5" transform="translate(-827 1479)" fill="url(#linear-gradient)"/>   
    
    <circle id="illusObjectBg" cx="44.294" cy="44.294" r="44.294" transform="translate(-798.652 1506.86)" fill="url(#linear-gradient-4)"/>
    
    <path id="Path_439" data-name="Path 439" d="M-10.658-31.875H10.658a12.42,12.42,0,0,1,8.965,3.735,13.058,13.058,0,0,1,3.785,9.015,13.058,13.058,0,0,1-3.785,9.015,12.42,12.42,0,0,1-8.965,3.735A12.384,12.384,0,0,1,5.329-7.521,13.16,13.16,0,0,1,1.1-10.658H-1.1A13.16,13.16,0,0,1-5.329-7.521a12.384,12.384,0,0,1-5.329,1.146,12.42,12.42,0,0,1-8.965-3.735,13.058,13.058,0,0,1-3.785-9.015,13.058,13.058,0,0,1,3.785-9.015A12.42,12.42,0,0,1-10.658-31.875ZM-12.75-25.5v4.283h-4.283v4.184h4.283v4.283h4.283v-4.283h4.184v-4.184H-8.467V-25.5ZM7.471-19.125a3.1,3.1,0,0,0-2.291.9,3.1,3.1,0,0,0-.9,2.291,3.1,3.1,0,0,0,.9,2.291,3.041,3.041,0,0,0,2.241.9,3.041,3.041,0,0,0,2.241-.9,3.1,3.1,0,0,0,.9-2.291,3.1,3.1,0,0,0-.9-2.291A2.981,2.981,0,0,0,7.471-19.125ZM13.846-25.5a3.1,3.1,0,0,0-2.291.9,3.1,3.1,0,0,0-.9,2.291,3.1,3.1,0,0,0,.9,2.291,3.041,3.041,0,0,0,2.241.9,3.041,3.041,0,0,0,2.241-.9,3.1,3.1,0,0,0,.9-2.291,3.1,3.1,0,0,0-.9-2.291A2.981,2.981,0,0,0,13.846-25.5Z" transform="translate(-755 1571)" fill="#fff"/>
  </g>
   
    <polygon class="circle3_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />

    <polygon class="circle3_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>

    <polygon class="circle3_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle3_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
  </svg>
 -->