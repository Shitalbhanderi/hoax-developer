<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fff2e6"/>
      <stop offset="1" stop-color="#fffcf3"/>
    </linearGradient>
    <linearGradient id="linear-gradient-6" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ffaf73"/>
      <stop offset="1" stop-color="#fffecc"/>
    </linearGradient>
  </defs>
  <g id="Group_312" data-name="Group 312" transform="translate(-885 -1917)">
    <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(885 1917)" fill="url(#linear-gradient)"/>
    <ellipse id="animObjectBg" cx="43.511" cy="43.511" rx="43.511" ry="43.511" transform="translate(913.14 1945.14)" fill="url(#linear-gradient-6)"/>
   
  </g>
   <circle class="circle5_dot1" id="Ellipse_17" data-name="Ellipse 17" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(50 59.431)"/>
    <circle class="circle5_dot2" id="Ellipse_18" data-name="Ellipse 18" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(60 59.431)"/>
    <circle class="circle5_dot3" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.7"  transform="translate(70 59.431)"/>
    <circle class="circle5_dot4" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="transparent" fill="transparent" stroke-width="1.7"  transform="translate(55 55)"/>
</svg>


