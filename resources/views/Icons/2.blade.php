<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="143.631" height="143.631" viewBox="0 0 143.631 143.631">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.735" y2="0.895" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#efebff"/>
      <stop offset="1" stop-color="#ecfbff"/>
    </linearGradient>
    <linearGradient id="linear-gradient-3" x1="0.126" y1="0.023" x2="1.172" y2="1.301" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#9cb3ff"/>
      <stop offset="1" stop-color="#b0ffe9"/>
    </linearGradient>
  </defs>
  <g id="Group_290" data-name="Group 290" transform="translate(-885 -1318)">
    <ellipse id="Ellipse_5" data-name="Ellipse 5" cx="71.815" cy="71.815" rx="71.815" ry="71.815" transform="translate(885 1318)" fill="url(#linear-gradient)"/>
    <ellipse id="uiObjectBg" cx="43.876" cy="43.876" rx="43.876" ry="43.876" transform="translate(913 1346)" fill="url(#linear-gradient-3)"/>
  
  </g>
      <circle class="circle2_dot1" id="Ellipse_17" data-name="Ellipse 17" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5" transform="translate(50 59.431)"/>
      <circle class="circle2_dot2" id="Ellipse_18" data-name="Ellipse 18" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5" transform="translate(60 59.431)"/>
      <circle class="circle2_dot3" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.7" transform="translate(70 59.431)"/>
      <circle class="circle2_dot4" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="transparent" fill="transparent" stroke-width="1.7"  transform="translate(55 55)"/>

</svg>








