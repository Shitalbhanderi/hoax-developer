<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fff2e6"/>
      <stop offset="1" stop-color="#fffcf3"/>
    </linearGradient>
    <linearGradient id="linear-gradient-6" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#ffaf73"/>
      <stop offset="1" stop-color="#fffecc"/>
    </linearGradient>
  </defs>
  <g id="Group_312" data-name="Group 312" transform="translate(-885 -1917)">
    <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(885 1917)" fill="url(#linear-gradient)"/>
    <ellipse id="animObjectBg" cx="43.511" cy="43.511" rx="43.511" ry="43.511" transform="translate(913.14 1945.14)" fill="url(#linear-gradient-6)"/>
   <!--  <path id="Path_454" data-name="Path 454" d="M15.834-21.849H22.02v4.06H15.834Zm-39.243,0h6.186v4.06h-6.186ZM1.336-42.533v6.186h-4.06v-6.186Zm-16.625,5.219,4.35,4.35-2.9,2.9-4.35-4.35Zm24.841,4.35,4.35-4.35,2.9,2.9-4.35,4.35Zm-10.246.773a12.218,12.218,0,0,1,8.747,3.625,12.218,12.218,0,0,1,3.625,8.747,11.922,11.922,0,0,1-1.691,6.283A12.642,12.642,0,0,1,5.492-9.09v3.673a1.967,1.967,0,0,1-.58,1.45,2.353,2.353,0,0,1-1.45.677H-4.85A2.353,2.353,0,0,1-6.3-3.967a1.967,1.967,0,0,1-.58-1.45V-9.09a12.642,12.642,0,0,1-4.495-4.446,11.922,11.922,0,0,1-1.691-6.283,12.218,12.218,0,0,1,3.625-8.747A12.218,12.218,0,0,1-.694-32.191ZM3.462-1.261V.769a2.353,2.353,0,0,1-.677,1.45,2.353,2.353,0,0,1-1.45.677h-4.06a2.353,2.353,0,0,1-1.45-.677A2.353,2.353,0,0,1-4.85.769v-2.03ZM-2.724-7.447h4.06V-11.8A8.33,8.33,0,0,0,5.83-14.744a8.028,8.028,0,0,0,1.691-5.074,8.21,8.21,0,0,0-2.416-5.8,7.868,7.868,0,0,0-5.8-2.32,7.868,7.868,0,0,0-5.8,2.32,8.21,8.21,0,0,0-2.416,5.8,8.028,8.028,0,0,0,1.691,5.074A8.329,8.329,0,0,0-2.724-11.8Z" transform="translate(957.408 2008.533)" fill="#fff"/> -->
  </g>
   <circle class="circle5_dot1" id="Ellipse_17" data-name="Ellipse 17" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(50 59.431)"/>
    <circle class="circle5_dot2" id="Ellipse_18" data-name="Ellipse 18" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(60 59.431)"/>
    <circle class="circle5_dot3" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.7"  transform="translate(70 59.431)"/>
    <circle class="circle5_dot4" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="transparent" fill="transparent" stroke-width="1.7"  transform="translate(55 55)"/>
</svg>




<!-- 
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="148.019" height="148.019" viewBox="0 0 148.019 148.019">
  <defs>
    <linearGradient id="linear-gradient" x1="0.235" y1="0.083" x2="0.735" y2="0.895" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#efebff"/>
      <stop offset="1" stop-color="#ecfbff"/>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="0.126" y1="0.023" x2="1.172" y2="1.301" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#9cb3ff"/>
      <stop offset="1" stop-color="#b0ffe9"/>
    </linearGradient>
    <linearGradient id="linear-gradient-3" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#fedcdb"/>
      <stop offset="1" stop-color="#fff6f1"/>
    </linearGradient>
    <linearGradient id="linear-gradient-6" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#29D0E9"/>
      <stop offset="1" stop-color="#F4DCDC"/>
    </linearGradient>
  </defs>
  <g id="Group_276" data-name="Group 276" transform="translate(431 -1375)">
    <g id="Group_274" data-name="Group 274" transform="translate(-1076.484 342.917)">
    
      <g id="uiux" transform="translate(674.423 1060.523)">
        <g id="uiObject">
          <ellipse id="uiObjectBg" cx="45.216" cy="45.216" rx="45.216" ry="45.216" fill="url(#linear-gradient-2)"/>
          <g id="uiObjectDesktop" transform="translate(26.541 31.356)">
            <g id="Group_203" data-name="Group 203" transform="translate(0 0)">
              <g id="ui-Rectangle-14" transform="translate(0 0)">
                <rect id="Rectangle_28" data-name="Rectangle 28" width="39.391" height="27.719" rx="2"/>
                <rect id="Rectangle_29" data-name="Rectangle 29" width="39.391" height="27.719" rx="2" fill="#fff"/>
              </g>
              <path id="ui-Rectangle-14-Copy-2" d="M2.918,0H36.473a2.918,2.918,0,0,1,2.918,2.918V7.295H0V2.918A2.918,2.918,0,0,1,2.918,0Z" transform="translate(0 0)" fill="#d6e5ff" fill-rule="evenodd"/>
            </g>
          </g>
          <g id="uiObjectPhone" transform="translate(43.75 24.627) rotate(0.074)">
            <g id="Group_204" data-name="Group 204">
              <rect id="ui-Rectangle-14-Copy" width="24.839" height="29.222" rx="2.003" fill="#4c82de"/>
              <ellipse id="Oval-17" cx="2.192" cy="2.192" rx="2.192" ry="2.192" transform="translate(10.228 20.455)" fill="#fff"/>
            </g>
          </g>
        </g>
      </g>
      <g id="Group_205" data-name="Group 205" transform="translate(649.102 1047.014)">
      
      </g>
    </g>
    <g id="Group_275" data-name="Group 275" transform="translate(-47 16)">
      <ellipse id="Ellipse_1" data-name="Ellipse 1" cx="73.39" cy="73.39" rx="73.39" ry="73.39" transform="translate(-384 1359)" fill="url(#linear-gradient)"/>
      <g id="Group_206" data-name="Group 206" transform="translate(-380.413 1373.931)">
       
      </g>
      <circle id="animObjectBg" cx="44.84" cy="44.84" r="44.84" transform="translate(-355 1388)" fill="url(#linear-gradient-6)"/>
    </g>
    <path id="Path_441" data-name="Path 441" d="M17.033-21.217h6.375v4.184H17.033Zm-40.441,0h6.375v4.184h-6.375Zm25.5-21.316v6.375H-2.092v-6.375Zm-17.133,5.379,4.482,4.482-2.988,2.988-4.482-4.482Zm25.6,4.482,4.482-4.482,2.988,2.988-4.482,4.482ZM0-31.875A12.591,12.591,0,0,1,9.015-28.14a12.591,12.591,0,0,1,3.735,9.015,12.287,12.287,0,0,1-1.743,6.475A13.028,13.028,0,0,1,6.375-8.068v3.785a2.027,2.027,0,0,1-.6,1.494,2.424,2.424,0,0,1-1.494.7H-4.283a2.424,2.424,0,0,1-1.494-.7,2.027,2.027,0,0,1-.6-1.494V-8.068a13.028,13.028,0,0,1-4.632-4.582,12.287,12.287,0,0,1-1.743-6.475A12.591,12.591,0,0,1-9.015-28.14,12.591,12.591,0,0,1,0-31.875ZM4.283,0V2.092A2.412,2.412,0,0,1,2.092,4.283H-2.092a2.424,2.424,0,0,1-1.494-.7,2.424,2.424,0,0,1-.7-1.494V0ZM-2.092-6.375H2.092v-4.482A8.584,8.584,0,0,0,6.724-13.9a8.274,8.274,0,0,0,1.743-5.229A8.461,8.461,0,0,0,5.977-25.1,8.109,8.109,0,0,0,0-27.492,8.109,8.109,0,0,0-5.977-25.1a8.461,8.461,0,0,0-2.49,5.977A8.274,8.274,0,0,0-6.724-13.9a8.584,8.584,0,0,0,4.632,3.038Z" transform="translate(-357.592 1467.533)" fill="#fff"/>
  </g>
     
    <circle class="circle5_dot1" id="Ellipse_17" data-name="Ellipse 17" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(50 59.431)"/>
    <circle class="circle5_dot2" id="Ellipse_18" data-name="Ellipse 18" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.5"  transform="translate(60 59.431)"/>
    <circle class="circle5_dot3" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="#transparent" fill="transparent" stroke-width="1.7"  transform="translate(70 59.431)"/>
    <circle class="circle5_dot4" id="Ellipse_19" data-name="Ellipse 19" cx="1" cy="1" r="1.5" stroke="transparent" fill="transparent" stroke-width="1.7"  transform="translate(55 55)"/>
</svg> -->
