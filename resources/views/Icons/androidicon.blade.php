<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#e4fff1"/>
      <stop offset="1" stop-color="#fbfff2"/>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#59dbb0"/>
      <stop offset="1" stop-color="#feffb5"/>
    </linearGradient>
  </defs>
  <g id="Group_309" data-name="Group 309" transform="translate(-509 -1345)">
    <ellipse id="Ellipse_22" data-name="Ellipse 22" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(509 1345)" fill="url(#linear-gradient)"/>
    <ellipse id="photoObjectBg" cx="43.511" cy="43.511" rx="43.511" ry="43.511" transform="translate(537 1373)" fill="url(#linear-gradient-2)"/>


   <!--  <path id="Path_412" data-name="Path 412" d="M6.375-25.5a2.027,2.027,0,0,1-1.494-.6,2.027,2.027,0,0,1-.6-1.494,2.027,2.027,0,0,1,.6-1.494,2.027,2.027,0,0,1,1.494-.6,2.027,2.027,0,0,1,1.494.6,2.027,2.027,0,0,1,.6,1.494,2.027,2.027,0,0,1-.6,1.494A2.027,2.027,0,0,1,6.375-25.5Zm-12.75,0a2.027,2.027,0,0,1-1.494-.6,2.027,2.027,0,0,1-.6-1.494,2.027,2.027,0,0,1,.6-1.494,2.027,2.027,0,0,1,1.494-.6,2.027,2.027,0,0,1,1.494.6,2.027,2.027,0,0,1,.6,1.494,2.027,2.027,0,0,1-.6,1.494A2.027,2.027,0,0,1-6.375-25.5ZM8.766-35.361l4.482-4.482-1.793-1.693L6.574-36.656A13.749,13.749,0,0,0,0-38.25a13.749,13.749,0,0,0-6.574,1.594l-4.881-4.881-1.793,1.693,4.482,4.482a15.5,15.5,0,0,0-4.433,5.18,14.19,14.19,0,0,0-1.644,6.773v2.191H14.842v-2.191A14.19,14.19,0,0,0,13.2-30.182,15.5,15.5,0,0,0,8.766-35.361Zm-23.607,24.7A14.657,14.657,0,0,0-10.559-.1,15.016,15.016,0,0,0,0,4.283,15.016,15.016,0,0,0,10.559-.1a14.657,14.657,0,0,0,4.283-10.559v-8.467H-14.842Z" transform="translate(579.842 1435.537)" fill="#fff"/> -->
    
  </g>
    <polygon class="circle1_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />
    <polygon class="circle1_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle1_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle1_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
</svg>















<!-- 



<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="146.779" height="146.779" viewBox="0 0 146.779 146.779">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#e4fff1"/>
      <stop offset="1" stop-color="#fbfff2"/>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#4D47CF"/>
      <stop offset="1" stop-color="#FFFFFF"/>
    </linearGradient>
  </defs>
  <g id="Group_272" data-name="Group 272" transform="translate(556 -944)">
    <ellipse id="Ellipse_23" data-name="Ellipse 23" cx="73.39" cy="73.39" rx="73.39" ry="73.39" transform="translate(-556 944)" fill="url(#linear-gradient)"/>
    <g id="Group_271" data-name="Group 271" transform="translate(-552.413 958.931)">
     
    </g>
    <circle id="photoObjectBg" cx="44.84" cy="44.84" r="44.84" transform="translate(-527 973)" fill="url(#linear-gradient-2)"/>
    <path id="Path_438" data-name="Path 438" d="M6.375-25.5a2.027,2.027,0,0,1-1.494-.6,2.027,2.027,0,0,1-.6-1.494,2.027,2.027,0,0,1,.6-1.494,2.027,2.027,0,0,1,1.494-.6,2.027,2.027,0,0,1,1.494.6,2.027,2.027,0,0,1,.6,1.494,2.027,2.027,0,0,1-.6,1.494A2.027,2.027,0,0,1,6.375-25.5Zm-12.75,0a2.027,2.027,0,0,1-1.494-.6,2.027,2.027,0,0,1-.6-1.494,2.027,2.027,0,0,1,.6-1.494,2.027,2.027,0,0,1,1.494-.6,2.027,2.027,0,0,1,1.494.6,2.027,2.027,0,0,1,.6,1.494,2.027,2.027,0,0,1-.6,1.494A2.027,2.027,0,0,1-6.375-25.5ZM8.766-35.361l4.482-4.482-1.793-1.693L6.574-36.656A13.749,13.749,0,0,0,0-38.25a13.749,13.749,0,0,0-6.574,1.594l-4.881-4.881-1.793,1.693,4.482,4.482a15.5,15.5,0,0,0-4.433,5.18,14.19,14.19,0,0,0-1.644,6.773v2.191H14.842v-2.191A14.19,14.19,0,0,0,13.2-30.182,15.5,15.5,0,0,0,8.766-35.361Zm-23.607,24.7A14.657,14.657,0,0,0-10.559-.1,15.016,15.016,0,0,0,0,4.283,15.016,15.016,0,0,0,10.559-.1a14.657,14.657,0,0,0,4.283-10.559v-8.467H-14.842Z" transform="translate(-482.158 1036.537)" fill="#fff"/>
  </g>
     <polygon class="circle1_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />

    <polygon class="circle1_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>

    <polygon class="circle1_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle1_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>

</svg>
 -->