<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="142.428" height="142.428" viewBox="0 0 142.428 142.428">
  <defs>
    <linearGradient id="linear-gradient" x1="0.276" y1="0.076" x2="0.757" y2="0.938" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#e4fff1"/>
      <stop offset="1" stop-color="#fbfff2"/>
    </linearGradient>
    <linearGradient id="linear-gradient-2" x1="0.018" y1="-0.116" x2="1" y2="1" gradientUnits="objectBoundingBox">
      <stop offset="0" stop-color="#59dbb0"/>
      <stop offset="1" stop-color="#feffb5"/>
    </linearGradient>
  </defs>
  <g id="Group_309" data-name="Group 309" transform="translate(-509 -1345)">
    <ellipse id="Ellipse_22" data-name="Ellipse 22" cx="71.214" cy="71.214" rx="71.214" ry="71.214" transform="translate(509 1345)" fill="url(#linear-gradient)"/>
    <ellipse id="photoObjectBg" cx="43.511" cy="43.511" rx="43.511" ry="43.511" transform="translate(537 1373)" fill="url(#linear-gradient-2)"/>


  </g>
    <polygon class="circle1_dot1" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="#ff87ab" />
    <polygon class="circle1_dot2" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle1_dot3" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
    <polygon class="circle1_dot4" points="2 2,2 2,4 0,2 -2,0 0" id="Ellipse_26" data-name="Ellipse 26" transform="translate(50 59.431)" fill="transparent" stroke="transparent" stroke-width="1.5"/>
</svg>













