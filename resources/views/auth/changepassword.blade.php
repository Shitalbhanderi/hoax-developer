<div class="modal fade" id="changePassword_model">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ html()->form()->id('changePasswordForm')->acceptsFiles()->open() }}
                {{ csrf_field() }}
                @method('POST')
                <div class="modal-header">
                    <h4 class="modal-title">Change Password</h4>
                    <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" style="position: relative; padding: 15px;">
                    <div id="has-error"></div>
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                    @endif
                    <div class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                        <input placeholder="current-Password" id="current-password" type="password" class="form-control" name="current-password"
                        required> @if ($errors->has('current-password'))
                        <span class="help-block"><strong>{{ $errors->first('current-password') }}</strong>
                        </span> @endif
                    </div>
                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                        <input placeholder="New Password" id="new-password" type="password" class="form-control" name="new-password" required>                        @if ($errors->has('new-password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('new-password') }}</strong>
                        </span> @endif
                    </div>
                    <div class="form-group">
                        <input placeholder="Confirm New Password" id="new-password-confirm" type="password" class="form-control" name="new-password_confirmation"
                        required>

                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">

                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Change Password</button>
                    <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
                </div>
            <!-- </form> -->
            {{ html()->form()->close() }}
        </div>
    </div>
</div>