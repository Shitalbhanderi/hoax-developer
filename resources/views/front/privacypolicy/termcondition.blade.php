<!DOCTYPE html>
<html>
<head>
	<title>Hoax Developer</title>
	 <link rel="icon" href="/img/title_logo.png" type="image/png" sizes="16x16">
    <title>Hoax Developer</title>
	<style type="text/css">
		@font-face {
		    font-family: Medium;
		    src: url('/assets/fonts/Montserrat-Medium.otf');
		}
		p{
			font-family: 'Medium';
			font-weight:400; 
		}
		b{
			font-family: 'Medium';
		}
		h2{
			font-family: 'Medium';
		}
		h3{
			font-family: 'Medium';
		}
		h4{
			font-family: 'Medium';
		}
	</style>
</head>
<body>
<div class="container">
<h2>Terms and Conditions</h2>
<h4><u>Latest Update: 18-April-2019</h4></u>
 
 
<p>These Terms and Conditions ("Terms", "Terms and Conditions") govern your relationship with www.hoaxdevelopers.com website (the "Service") operated by Jay Gadesha. Please read these Terms and Conditions carefully before using the Service. Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service. </p>
 
<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p> 
 
<h3>Intellectual Property</h3> 
 
<p>The Service and its original content, features, and functionality are and will remain the exclusive property of Hoax Developers.<p> 
 
<h3>Links To Other WebSites </h3>
 
<p>Our Service may contain links to third-party websites or services that are not owned or controlled by Hoax Developers. </p>
 
<p>Hoax Developers has no control over and assumes no responsibility for, the content, privacy policies, or practices of any third party websites or services.   You further acknowledge and agree that Hoax Developers shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with the use of or reliance on any such content, goods or services available on or through any such websites or services.</p>
 
<p>We strongly advise you to read the terms and conditions and privacy policies of any third-party websites or services that you visit. </p>
 
<h3>Termination </h3>
 
<p>We may terminate or suspend your access immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms. Upon termination, your right to use the Service will immediately cease. </p>

<h3>Limitation Of Liability </h3>
 
<p>In no event shall Hoax Developers, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from  
<p>(i) your access to or use of or inability to access or use the Service;</p>
<p>(ii) any conduct or content of any third party on the Service; </p>
<p>(iii) any content obtained from the Service; and </p>
<p> (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p></p> 
 
<h3>Disclaimer </h3>
 
<p>Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance. Hoax Developers its subsidiaries, affiliates, and its licensors do not warrant that 
<p> a) any errors or defects will be corrected; </p>
<p> b) the results of using the Service will meet your requirements.</p> </p>
 
<h3>Changes </h3>
 
<p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. By using our Service, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p> 
 
<h3>Contact Us </h3>
 
<p>If you have any questions about these Terms, please contact us  At hoax developers@gmail.com </p>  
	
</div>
</body>
</html>