{{ html()->form()->id('contactusForm')->acceptsFiles()->open() }}
{{ csrf_field() }}
@method('POST')
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{
        html()
        ->input('text', 'fullname')
        ->class('form-control fullname')
        ->id('fullname')
        ->placeholder('Enter Full Name *')

      }}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{
        html()
        ->input('email', 'email')
        ->class('form-control email')
        ->id('email')
        ->placeholder('Enter Email Address *')
      }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{
        html()
        ->input('text', 'skype_id')
        ->class('form-control skype_id')
        ->id('skype_id')
        ->placeholder('Enter Skypeid *')
      }}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{
        html()
        ->input('text', 'country')
        ->class('form-control country')
        ->id('country')
        ->placeholder('Enter Country')
      }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
      {{
        html()
        ->input('text', 'project_type')
        ->class('form-control project_type')
        ->id('project_type')
        ->placeholder('Enter Project Type')
      }}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{
        html()
        ->input('text', 'budget')
        ->class('form-control budget')
        ->id('budget')
        ->placeholder('Enter your budget')
      }}
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12">
    <div class="form-group">
      {{
        html()->file('attachment')
        ->style(['border' => '1','padding'=>'0'])
        ->class('form-control attachment')
        ->id('attachment')

      }}
    </div>

  </div>
  
</div>
<div class="row">
  <div class="col-lg-12">
   <div class="form-group">
    {{
      html()->textarea('description')
      ->style(['border' => '1'])
      ->class('form-control description')
      ->id('description')
      ->placeholder('Brief Project Description... *')
      ->attribute('rows', 7)
    }}
  </div>
</div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="success_msg" id="success_msg"></div>
  </div>
</div>
<div class="col-lg-12">
  <button type="submit" class="btn" id="submit"><i id="submit_load"></i> SEND QUERY</button>
</div> 
{{ html()->form()->close() }}  
<div id="has-error" style="width: 100%"></div> 

@section('page-js')
<script type="text/javascript">
 $(document).ready(function(){
   $('#contactusForm').on('submit', function(event){  
     $('form').find('.help-block').remove();
     $('form').find('.alert').removeClass('alert alert-danger');
     $('form').find('.form-group').removeClass('has-error');
     $('.alert-success').remove();
     $("#submit").attr('disabled', 'disabled');

     $("#submit_load").addClass('fa fa-spinner fa-spin');
     event.preventDefault();
     $.ajax({
       url:"{{route('contactus.store')}}",
       method:"POST",
       data:new FormData(this),
       dataType:'JSON',
       contentType: false,
       cache: false,
       processData: false,
       success: function (data) {    
       },
       error: function (response) {
       },
       complete: function(){
        $('#submit').attr("disabled", false);
        $("#submit_load").removeClass('fa fa-spinner fa-spin');
      }
    })
     .done(function(data) {
       $('.success_msg').append('<div class="alert alert-success" role="alert">Thank you! Your mail has been sent successfully.</div>');
       $("#contactusForm")[0].reset();
     })
     .fail(function(data){
      $('#submit').attr("disabled", false);
      $("#submit_load").removeClass('fa fa-spinner fa-spin');
      var errors = data.responseJSON;
      if ($.isEmptyObject(errors) == false) {
       $.each(errors.errors, function (key, value) {
        $('#' + key)
        .closest('.form-group')
        .addClass('has-error')
        .append('<span class="help-block"><strong>' + value + '</strong></span>');
      });
     }
   });
   });
 });
</script>
<script type="text/javascript">
  var uploadField = document.getElementById("attachment");
  uploadField.onchange = function() {
    $(this).closest('.row').removeClass('has-error');
    $(this).closest('.row').find('.help-block').remove();
    if(this.files[0].size > 2097152){
      console.log(uploadField)
      $(this).closest('.row')
      .addClass('has-error')
      .append('<span class="help-block" style="float:left;"><strong style="color:#a9444;">' + "* Image size must be less than 2 MB" + '</strong></span>');
      this.value = "";
    };
  };
</script>
@stop