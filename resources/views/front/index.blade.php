@extends('front.layout.header')
@section('page-css')
<style type="text/css">
strong{
	color:red;
	float: left;
}
.read-more-blog{
	margin-left: 45%;
	margin-top:70px;
}
.read-more-blog a{
	background-color: #5163ED;
	color:#fff;
}
</style>

@stop
<!-- Hero Area Start -->

@section('hero-section')
<div id="hero-area" class="hero-area-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12" style="padding-left: 20px;">
				<div class="contents">
					<h1 class="head-title" style="word-wrap: break-word;">{{$home->head_title}}
					</h1>
					<p class="herofont">{{$home->hero_description}}</p>
					<div class="header-button">

						<a rel="nofollow" a href="{!! url( $home->header_button_link) !!}" target="_blank"><img src={{$home->header_button_url}}></a>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
			<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
				<div class="intro-img">
					<img class="img-fluid" src={{$home->intro_img_url}}>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
<!-- Hero Area End -->


@section('content')
<!-- Services Section Start -->
<section id="services" class="section-padding">
	<div class="container">
		<div class="section-header text-center">
			<h5 class="wow fadeInDown">
				{{$services[0]->section_header}}
			</h5>
			<h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
				{{$services[0]->section_title}}
			</h1>
		</div>
		<div class="row">
			@foreach($services as $key => $service)
			<div class="col-md-6 col-lg-4 col-xs-12">
				<div class="services-item wow fadeInRight service-item-{{$service->id}}" data-wow-delay="0.3s">
					<div class="services-item-icon">
						<i class="">
							<div class="service-item-img">
								@include('Icons.'.$service->id)
								<img id="" class="" height="50px" width="50px" src={{$service->service_item_icon_url}} />
							</div>
						</i>
					</div>
					<div class="services-content">
						<h3><a target="_blank">{{$service->service_item_content_heading}}</a>
						</h3>

						<p>{{$service->service_item_content_description}}</p>

					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>
<!-- Service Section End -->

<!-- Portfolio Section Start -->
<section id="portfolio" class="section-padding">
	<div class="container">
		<div class="section-header text-center">
			<h5 class="wow fadeInDown">
				{{$portfolios[0]->section_header}}
			</h5>
			<h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
				{{$portfolios[0]->section_title}}
			</h1>
		</div>
		<div class="row">
			@php $count=$add=3;
			@endphp @foreach($portfolios as $key=>$portfolio) @if($key+1 == $count)
			<div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">
				@php $portfolio_image=$portfolio->portfolio_item_image_big_url; $add=($add == 1)?5:1; $count += $add;
				@endphp @else
				<div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
					@php $portfolio_image=$portfolio->portfolio_item_image_small_url;
					@endphp @endif
					<div class="work-item wow bounceIn data-wow-delay="0.3s">
						<img src="{{$portfolio_image}}" class="image work-item-img">
						<div class="text-block">
							<h2 style="color:{{$portfolio->portfolio_item_name_color}}">{{$portfolio->portfolio_item_name}}</h2>
						</div>
						<div class="work-item-store">
							<div class="item-icon">
								@if($portfolio->portfolio_item_andriod)
								<a href="{!! url($portfolio->portfolio_item_andriod_url) !!}" target="_blank">
									  <i class="hovicon effect-1 sub-a"><i class='fab fa-google-play'></i></i>
								</a>
								@endif
								@if($portfolio->portfolio_item_ios)
								<a href="{!! url($portfolio->portfolio_item_ios_url) !!}" target="_blank">
									<i class="hovicon effect-1 sub-a"><i class='fab fa-app-store-ios'></i></i>
								</a>
								@endif
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</section>
	<!-- Portfolio Section End -->

	<!-- Testimonial Section Start -->
<!-- 	<section id="testimonial" class="testimonial section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
				  <section class="regular slider">
                  <div class="slider-item testimonial-item">
                     <div class="testimonial-img-thumb">
                      <img src="assets/img/testimonial/img1.jpg" alt="">
                    </div>
                    <div class="testimonial-info">
                      <h2>David Smith</h2>
                      <h3>CEO,CryptoExchange Foundation</h3>
                    </div>
                    <div class="testimonial-content">
                      <p class="description">"9 Brainz has exceeded my expectations from design to development,and i love now i can trust them with product strategy too."</p>
                    </div>
                  </div>
              </section>
          </div>
      </div>
  </div>
</section> -->
<!-- Testimonial Section End -->

<!-- Blog Section Start -->
<section id="blog" class="section-padding">
	<div class="container">
		<div>
			<div class="section-header text-center">
				<h5 class="wow fadeInDown">
					{{$blogs[0]->section_header}}
				</h5>
				<h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
					{{$blogs[0]->section_title}}
				</h1>
			</div>
			@foreach($blogs as $key => $blog)
			@if($key<=1)
			<div class="row">
				<div class="blog wow {{(($key%2)==0)?'slideInRight':'slideInLeft'}} data-wow-delay="0.3s"">
					<div class="col-xs-12 col-sm-12 col-md-12 clo-lg-6" style="">
						<div class=" {{(($key%2)==0)?'blog-left':'blog-right'}}">
							<img class="image blog-img" src={{$blog->blog_item_medium_image_url}}>
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 clo-lg-6" style="">
						<div class=" {{(($key%2)==0)?'blog-right':'blog-left'}}">
							<div class="blog-content">
								<div class="content-heading-small-card">
									<h3>{{$blog->blog_item_content_heading }}</h3>
								</div>
								<div class="content-body-small-card">
									<p id=blog-description> {!! $blog->blog_item_content_description !!}</p>
								</div>
							</div>
							<div class="row">
								<div class="post-meta-small-card text-center">
									<div class="author mr-2"><img class="img-thumbnail" src="/defaultimage/user-defualt.jpg" alt="Colorlib">
										<span class="name"> {{str_limit($blog->auther,15)}} </span>
									</div>
									<div class="mr-2 date date_responsive">{{$blog->created_at->format('d F, Y')}}</div>
								</div>
							</div>
							<div>
								<div class="text-center-blog">
									<a href="{{route('blog-readmore',$blog->id)}}" target="_blank" class="btn btn-lg">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			@endforeach
			<div>
				<div class="text-center-blog" style="text-align: center;padding-top: 50px">
					<a href="{{route('blog-viewall')}}" target="_blank" class="btn btn-lg">View All</a>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- Blog Section End -->
<!-- Contact section Start -->
<section id="contact" class="section-padding">
	<div class="container">
		<div class="section-header text-center">
			<h5 class="wow fadeInDown">CONTACT</h5>
			<h2 class="section-title wow fadeInDown" data-wow-delay="0.3s">
			Say Hello To Us</h2>
		</div>
		<div class="row">
			<div class="contact">
				@include('front.contactus.index')
			</div>
		</div>
	</div>
</section>
<!-- end of contact us section -->

@stop

@section('footer')

<footer id="footer" class="footer-area section-padding">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 col-mb-12">
				<div class="widget">
					<h3 class="footer-logo">
						<img src={{$contact->logo_url}} alt="" height="55px" width="160px">
					</h3>
					<div class="textwidget">
						<p>{{$contact->footer_text}}</p>
					</div>
					<div class="social-icon">
						<input type="hidden" id={{sizeof($sociamedias)}} name="arr_size" class="arr_size"> @foreach($sociamedias as $socialmedia)
						<a href="{!! url($socialmedia->url) !!}" target="_blank"><img src={{$socialmedia->icon_url}}></a> @endforeach
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6">
				<h3 class="footer-titel">Sitemap</h3>
				<ul class="footer-link">
					<li><a href="#services">Services</a></li>
					<li><a href="#portfolio">Portfolio</a></li>
					<li><a href="#blog">Blog</a></li>
					<!-- <li><a href="#testimonial">Testimonials</a></li> -->
					<li><a href="#contact">Contact</a></li>
				</ul>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
	          <h3 class="footer-titel">What We Provide</h3>
	          <ul class="footer-link">
	            @foreach($services as $service)
	            <li>{{$service->service_item_content_heading}}</li>
	            @endforeach
	          </ul>
       	 	</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<h3 class="footer-titel">Contact Us</h3>
				<ul class="footer-link">
					<li>
						<a href="mailto:hoaxdevelopers@gmail.com" target="_blank">
						<i class='far fa-envelope'></i>hoaxdevelopers@gmail.com</a>
					</li>
					<li>
						<a href="skype:{{$contact->skype_id}}?chat"><i class='fab fa-skype'></i> {{$contact->skype_id}}</a>
					</li>
				</ul>
			</div>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="text-center">
					<hr style="height:1px; width: 80%; border:none;  background-color:#808080;">
					<p>&copy;2019-2021 HOAX DEVELOPERS. ALL RIGHTS RESEVRED.</p>
				</div>
			</div>
		</div>
	</div>
</footer>

@stop
@section('page-js')

@stop