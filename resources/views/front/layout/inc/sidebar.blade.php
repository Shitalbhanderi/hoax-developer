<!-- Navbar Start -->
<nav class="navbar navbar-expand-md bg-inverse fixed-top scrolling-navbar">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <a href="{{route('index')}}" class="navbar-brand">
      <img height="45px" wight="100%" src={{$contact->logo_url}}></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse"
      aria-expanded="false" aria-label="Toggle navigation">
      <i class="lni-menu"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto w-100 justify-content-end clearfix">

      
       @php
        if(Request::route()->getName()=='blog-readmore' || Request::route()->getName()=='blog-viewall' ){
            $route=[
              'home' => route('index')."#home",
              'services' => route('index')."#services",
              'portfolio' => route('index')."#portfolio",
              'blog' => route('index')."#blog",
              'contact' => route('index')."#contact",
            ];
            $active_link='blog';
            $offset=1;

        }else{
            $route=[
              'home' => "#home",
              'services' => "#services",
              'portfolio' => "#portfolio",
              'blog' => "#blog",
              'contact'=>'#contact',
            ];
            $offset=0;
        }
       @endphp
        <li class="nav-item {{(isset($active_link) && ($active_link=='blog'))?'':''}}">
          <a id="{{($offset==0)?'0':'1'}}" class="nav-link home" href="{{$route['home']}}">
            HOME
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{$route['services']}}">
            SERVICES
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{$route['portfolio']}}">
            PORTFOLIO
          </a>
        </li>
        <!--  <li class="nav-item">
          <a class="nav-link" href="#testimonial">
            TESTIMONIAL
          </a>
        </li> -->
        <li class="nav-item {{(isset($active_link) && ($active_link=='blog'))?'active':''}}">
          <a class="nav-link" href="{{$route['blog']}}">
            BLOG
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{$route['contact']}}">
           CONTACT
         </a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- Navbar End -->