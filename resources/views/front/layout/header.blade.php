<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="/defaultimage/title-logo.png" type="image/png" sizes="16x16">
    <title>Hoax Developers</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <!-- Icon -->
    <link rel="stylesheet" href="/assets/fonts/line-icons.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/assets/css/owl.theme.css">

    <link rel="stylesheet" href="/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="/assets/css/nivo-lightbox.css">
    <!-- Animate -->
    <link rel="stylesheet" href="/assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" href="/assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'crossorigin='anonymous'>
    <style type="text/css">
    .nav-item a {
        color: black;
    }
    </style>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-138694888-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-138694888-1');
    </script>
@yield('page-css')
</head>

<body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
        <!-- Navbar Start -->
        @include('front.layout.inc.sidebar')
        <!-- Navbar End -->

        <!-- Hero Area Start -->
        @yield('hero-section')
        <!-- Hero Area End -->
    </header>

    @yield('content')
    
    @yield('footer')

    <a href="#" class="back-to-top" style="display: inline;">
        <i class="lni-arrow-up"></i>
    </a>

    <div id="preloader" style="display: none;">
      <div class="loader" id="loader-1"></div>
  </div>

  <script src="/assets/js/jquery-min.js"></script>
  <script src="/assets/js/popper.min.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/owl.carousel.min.js"></script>
  <script src="/assets/js/wow.js"></script>
  <script src="/assets/js/jquery.nav.js"></script>
  <script src="/assets/js/scrolling-nav.js"></script>
  <script src="/assets/js/jquery.easing.min.js"></script>
  <script src="/assets/js/jquery.counterup.min.js"></script>
  <script src="/assets/js/waypoints.min.js"></script>
  <script src="/assets/js/main.js"></script>

  <script src="/assets/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->
  <script type="text/javascript">
     $(window).load(function() {
        $(".loader").fadeOut("slow");
        var uri = window.location.toString();
        if (uri.indexOf("#home") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("#home"));
            window.history.replaceState({}, document.title, clean_uri);
        }else if (uri.indexOf("#services") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("#services"));
            window.history.replaceState({}, document.title, clean_uri);
        }else if (uri.indexOf("#portfolio") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("#portfolio"));
            window.history.replaceState({}, document.title, clean_uri);
        }else if (uri.indexOf("#blog") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("#blog"));
            window.history.replaceState({}, document.title, clean_uri);
        }else if (uri.indexOf("#contact") > 0) {
            var clean_uri = uri.substring(0, uri.indexOf("#contact"));
            window.history.replaceState({}, document.title, clean_uri);
        }
    });
</script>

@yield('page-js')



</body>

</html>