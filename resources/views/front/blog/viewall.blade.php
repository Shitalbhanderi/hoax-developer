@extends('front.layout.header') 
@section('page-css') 
<style type="text/css">
strong{
	color:red;
	float: left;
}
.read-more-blog{
	margin-left: 45%;
	margin-top:70px;
}
.read-more-blog a{
	background-color: #5163ED;
	color:#fff;
} 
.section-header {
    padding-top: 95px;
}


</style>

@stop
<!-- Hero Area Start -->


<!-- Hero Area End -->


@section('content')
<!-- Services Section Start -->

<!-- Service Section End -->

<!-- Portfolio Section Start -->

	
<!-- Blog Section Start -->
<section id="blog" class="section-padding">
	<div class="container">
		<div>
			<div class="section-header text-center">
				<h5 class="wow fadeInDown">
					{{$blogs[0]->section_header}}
				</h5>
				<h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
					{{$blogs[0]->section_title}}
				</h1>
			</div>
		



			<div class="row">

				@foreach($blogs as $key => $blog)
				
				<div class="blog">
					<div class="col-xs-12 col-sm-12 col-md-12 clo-lg-6" style="">
						<div class=" {{(($key%2)==0)?'blog-left':'blog-right'}}">
							<img class="image blog-img" src={{$blog->blog_item_medium_image_url}}>
						</div>
					</div> 

					<div class="col-xs-12 col-sm-12 col-md-12 clo-lg-6" style="">
						<div class=" {{(($key%2)==0)?'blog-right':'blog-left'}}">
							<div class="blog-content">
								<div class="content-heading-small-card">
									<h3>{{$blog->blog_item_content_heading }}</h3>
								</div>
								<div class="content-body-small-card"> 
									<p id=blog-description> {!! $blog->blog_item_content_description !!}</p>
								</div>

							</div>
							<div class="row">
								<div class="post-meta-small-card text-center">
									<div class="author mr-2"><img class="img-thumbnail" src="/defaultimage/user-defualt.jpg" alt="Colorlib">
										<span class="name"> {{str_limit($blog->auther,15)}} </span>
									</div>
									<div class="mr-2 date date_responsive">{{$blog->created_at->format('d F, Y')}}</div>
								</div>
							</div>
							<div>									
								<div class="text-center-blog">
									<a href="{{route('blog-readmore',$blog->id)}}" target="_blank" class="btn btn-lg">Read More</a>
								</div>
							</div>

						</div>
					</div> 
				</div>
				
				@endforeach
			</div>
			
		</div>
	</div>

</section>
<!-- Blog Section End -->
<!-- Contact section Start -->

@stop


@section('page-js')

@stop