@extends('front.layout.header') 
@section('page-css')
<style type="text/css">
.blog-readmore {
	text-align: left;
	box-shadow: 0 0 12px #F4F4F4;
	transition: all 0.3s ease-in-out 0s;
	-moz-transition: all 0.3s ease-in-out 0s;
	-webkit-transition: all 0.3s ease-in-out 0s;
	-o-transition: all 0.3s ease-in-out 0s;
	box-shadow: 0 5px 12px 5px rgba(0, 0, 0, 0.08);
	overflow: hidden;
}
.blog-readmore-main{
	margin-top:60px;
}

.blog-readmore-small {
	border-radius: 10px;
}

.blog-readmore-image {
	width: 100%;
}

.blog-readmore-content {
	padding: 40px;
	padding-top: 50px;
}

.post-meta {
	padding-top: 20px;
	color: #9EA0A4;
}

.author img {
	width: 40px;
	border-radius: 50%;
	display: inline-block;
}

.date {
	float: right;
	padding-top: 5px;
}

.name {
	padding-left: 10px;
}

.content-body {
	padding-top: 20px;
	margin-top: 20px;
}

.content-body h5 {
	padding-bottom: 10px;

}

.content-footer {
	float: right;
	padding-bottom: 10px;
}

.circle-icon {
	border: none;
	background-color: #000;
	padding: 8px;

	border-radius: 50%;
	width: 30px;
	height: 30px;
}

.row {
	padding-top: 40px;
}

.small-cards {
	padding: 10px;
}
.blog-readmore-small{
	padding-bottom: 10px;
}
.content-heading-small-card{
    display: -webkit-box!important;
    -webkit-line-clamp: 2;
    -moz-line-clamp: 2;
    -ms-line-clamp: 2;
    -o-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    -ms-box-orient: vertical;
    -o-box-orient: vertical;
    box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: normal;
    height: 50px;
    padding-left: 20px; 
    /*padding-right: 20px; */

}
.content-body-small-card { 
	padding-top: 30px;  
    display: -webkit-box!important;
    -webkit-line-clamp: 5;
    -moz-line-clamp: 5;
    -ms-line-clamp: 5;
    -o-line-clamp: 5;
    line-clamp: 5;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    -ms-box-orient: vertical;
    -o-box-orient: vertical;
    box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: normal;
    height: 153px; 
    /*padding-right: 20px; */
}

.post-meta-small-card {
	padding: 0px;
	padding-top: 10px;
	padding-bottom: 10px;
	color: #9EA0A4;
}
.more-posts {
	margin-top: 60px;
}
.date_responsive{
	float: right; 
    padding-top: 5px; 
}
</style>
@stop
@section('content')
<section id="blog" class="section-padding">
	<div class="container">
		<div class="row">
			<div class="blog-readmore blog-readmore-main">

				<div class="blog-readmore-image">
					<img src={{$blog->blog_item_big_image_url}}>
				</div>
				<div class="blog-readmore-content">
					<h2>
						<b>
						{{$blog->blog_item_content_heading}}
						</b>
				</h2>
				<div class="post-meta">
					<span class="author mr-2"><img class="img-thumbnail" src="/defaultimage/user-defualt.jpg" alt="Colorlib">
						<span class="name">{{$blog->auther}}</span>
					</span>
					<span class="mr-2 date">{{$blog->created_at->format('d F, Y')}}</span>
				</div>
				<div class="content-body">
					<p>
						{!! $blog->blog_item_content_description !!}
					</p>
				</div>
				</div>

			</div>
		</div>
		<div class="more-posts">
		@foreach($blogs as $key=>$blog)
		@if($key<=0)
			<h4>More Posts</h4>
		@endif
		@endforeach
			<div class="row">
				@foreach($blogs as $key=>$blog)
				<div class="col-md-4">
					<div class="blog-readmore blog-readmore-small">
						<div class="blog-readmore-image">
							<img src={{$blog->blog_item_small_image_url}}>
						</div>
						<div class="blog-readmore-content-small small-cards">
							<div class="content-heading-small-card">
								<h5>
								<a href="{{route('blog-readmore',$blog->id)}}" style="text-decoration: none;color:black">{{$blog->blog_item_content_heading}}</a>
								</h5>
							</div>
							<div class="content-body-small-card">
								{!! $blog->blog_item_content_description !!}
							</div>


							<div class="post-meta-small-card">
								<span class="author mr-2"><img class="img-thumbnail" src="/defaultimage/user-defualt.jpg" alt="Colorlib">
									<span class="name"> {{str_limit($blog->auther,5)}} </span>
								</span>
								<span class="mr-2 date_responsive">{{$blog->created_at->format('d F, Y')}}</span>
							</div>

						</div>
					</div>
				</div>
				@endforeach


			</div>
		</div>
	</section>
@stop





































































	@section('page-js') 
	@stop