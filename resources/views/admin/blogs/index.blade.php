@extends('admin.layout.index') 
@section('title') Blogs 
@stop 
@section('page-css')
<link rel="stylesheet" href="/assets/css/main.css">
<style type="text/css">
.blog {
  width: 950px;
}
.add-blog{
  float: right;
  margin-right: 100px;
}
.post-meta-small-card{
  padding:15px;
}
.author{
  float:left;
}
.content-heading-small-card{
  height: 113px;
}
.blog-content{
  height: 69%;
}
.date_responsive{
  padding-right: 40px;
}
.blog-left{
  width: 100%;
  float: none;
}
.blog-right{
  width: 100%;
  float: none;
}
.blog-img{
  height: 653px;

}
.blog-content a {
  background-color: #594FEA;
  float: none; 
  margin-right: 0px; 
  color: #fff;
}
.blog p{
  margin-top: 2px;
}
.col-md-6,.col-sm-6,.col-xs-6,.col-lg-6{
  padding-right:0px  !important;
  padding-left: 0px  !important;
}
.post-meta-small-card{
  overflow: auto;
}
</style>
@stop 

@section('active-menu-icon')
<em class="fa fa-clone">&nbsp;</em> 
@stop 

@section('active-menu') Blogs 
@stop

@section('page-header') Blogs 
@stop 

@section('content')
<section id="blog" class="section-padding">
    <div class="section-header text-center">
      <h5 class="wow fadeInDown">
        {{$blogs[0]->section_header}}
        <a href="" id="{{$blogs[0]->id}}" data-toggle="modal" class="section_header_edit"> <i class='far fa-edit'></i></a>
      </h5>
      <h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
        {{$blogs[0]->section_title}}
        <a href="" id="{{$blogs[0]->id}}" data-toggle="modal" class="section_title_edit"> <i class='far fa-edit'></i></a>
      </h1>
    </div>
    <div class="text-center">
      <a href="" class="btn btn-primary add-blog-model" id="{{$blogs[0]->id}}" data-toggle="modal" style="background-color: #5176eb;">Add Blog</a>
    </div>
    @foreach($blogs as $key => $blog)
    <div class="row">
      <div class="blog">
        <a href="" id="{{$blog->id}}" data-toggle="modal" class="blog_item_edit"><i class='far fa-edit  {{(($key%2)==0)?'blog-edit-right':'blog-edit-right'}}'></i></a>

        <span class="deleteRecord" data-id="{{ $blog->id }}" ><i class='fa fa-trash blog-edit-right' style="color:#5176eb;"></i></span>

        <div class="col-xs-12 col-sm-12 col-md-6 clo-lg-6" style="">
          <div class=" {{(($key%2)==0)?'blog-left':'blog-right'}}">
            <img class="image blog-img" src={{$blog->blog_item_medium_image_url}}>
          </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 clo-lg-6" style="">
         <div class=" {{(($key%2)==0)?'blog-right':'blog-left'}}">
          <div class="blog-content">
            <div class=" {{(($key%2)==0)?'blog-right':'blog-left'}}">
              <div class="blog-content">
                <div class="content-heading-small-card">
                  <h3>{{$blog->blog_item_content_heading }}</h3>
                </div>
                <div class="content-body-small-card"> 
                  {!! $blog->blog_item_content_description !!}
                </div>
              </div>
              <div class="row">

                <div class="post-meta-small-card text-center">
                  <div class="author mr-2"><img class="img-thumbnail" src="/defaultimage/user-defualt.jpg" alt="Colorlib">
                    <span class="name"> {{str_limit($blog->auther,15)}} </span>
                  </div>
                  <div class="mr-2 date date_responsive">{{$blog->created_at->format('d F, Y')}}</div>
                </div>
              </div>
              <div>                 
                <div class="text-center-blog">
                  <a href="{{route('blog-readmore',$blog->id)}}" target="_blank" class="btn btn-lg">Read More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  @endforeach
  @include('admin.blogs.edit')
  @include('admin.blogs.add')
</section>
@stop 

@section('page-js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.js"></script>   

<!-- Open Particular fields to edit and summernote -->
<script>
  $(document).ready(function(){
    $(".section_header_edit").click(function(){
     $(".blog_item_model, .section_title_model").hide();
     $(".section_header_model").show();
   });
    $(".section_title_edit").click(function(){
     $(".blog_item_model, .section_header_model").hide();
     $(".section_title_model").show();
   });
    $(".blog_item_edit").click(function(){
      $(".section_header_model, .section_title_model").hide();
      $(".blog_item_model").show();
      $('.summernote').summernote({
        height: 150,
        toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol','paragraph']],
        ['Insert',['link','table','hr']],
        ],
        onInit: function() {
          $(".summernote").summernote('code', '<p style="font-family: Curlz MT;"><br></p>')
        }
      });
    });
  });
</script>

<!-- retrive data from database and show in model-->
<script>
  $(document).on('click', '.blog_item_edit,.section_header_edit,.section_title_edit', function(){
    var id = $(this).attr("id");
    $.ajax({
      url:"{{route('blogs.show',$blog->id)}}",
      method:'get',
      data:{id:id},
      dataType:'json',
      success:function(data)
      {
        $('#section_header').val(data.section_header);
        $('#section_title').val(data.section_title);
        $('#blog_item_content_heading').val(data.blog_item_content_heading);         
        $('#blog_item_content_description').summernote('code', data.blog_item_content_description);
        $('#auther').val(data.auther);
        $('#blog_model').modal('show');
        $('#blog_id').val(id);

        var textlable=document.getElementById("textlimit-header");
        textlimit('#section_header',30,textlable);
        var textlable=document.getElementById("textlimit-title");
        textlimit('#section_title',60,textlable);

        var textlable=document.getElementById("textlimit-itemheading");
        textlimit('#blog_item_content_heading',70,textlable);

        blog_item_content_description=document.getElementById("blog_item_content_description");
        textarearow(blog_item_content_description);

        var textlable=document.getElementById("textlimit-auther");
        textlimit('#auther',20,textlable);
      }
    })
  });
</script>

<!-- update data -->
<script>
  $(document).ready(function(){
   $('#blogsForm').on('submit', function(event){
    var id = $('#blog_id').val();

    submitData(id,'blog_submit','blogs');
    alert('ok');
    $("#blog_submit").attr('disabled', 'disabled');
    modelClose();
    $("#blog_submit").attr('disabled', 'disabled');
    event.preventDefault();
    $.ajax({
     url:'blogs/'+id,
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success: function (data) {
     },
     error: function (response) {
     },
     complete: function(){
      $('#blog_submit').attr("disabled", false);
    }
  })
    .done(function(data) {
     window.location.reload(true);
   })
    .fail(function(data){
      $('#blog_submit').attr("disabled", false);
      var errors = data.responseJSON;
      if ($.isEmptyObject(errors) == false) {
        $.each(errors.errors, function (key, value) {
         $('#' + key)
         .closest('.form-group')
         .addClass('has-error')
         .append('<span class="help-block"><strong>' + value + '</strong></span>');
       });
      }
    });
  });
 });

</script>

<!-- Retrive header and title for add new blog -->
<script>
  $(document).on('click', '.add-blog-model', function(){
    var id = $(this).attr("id");
    $.ajax({
      url:"{{route('blogs.create')}}",
      method:'get',
      data:{id:id},
      dataType:'json',
      success:function(data)
      {
        $('.summernote').summernote({
          height: 150,
          toolbar: [
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol','paragraph']],
          ['Insert',['link','table','hr']],
          ]
        });
        $('.section_header').val(data.section_header);
        $('.section_title').val(data.section_title);
        $('#add_blog_model').modal('show');
        var textlable=document.getElementById("textlimit-itemheading_add");
        textlimit('#blog_item_content_heading',70,textlable);         
        var textlable=document.getElementById("textlimit-auther_add");
        textlimit('#auther',20,textlable);
      }
    })
  });
</script>

<!-- Add blogs -->
<script>
  $(document).ready(function(){
   $('#add_blogsForm').on('submit', function(event){
    modelClose();
    $("#blog_submit").attr('disabled', 'disabled');
    event.preventDefault();
    $.ajax({
     url:'{{route('blogs.store')}}',
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success: function (data) {
     },
     error: function (response) {
     },
     complete: function(){
      $('#blog_submit').attr("disabled", false);
    }
  })
    .done(function(data) {
     window.location.reload(true);
   })
    .fail(function(data){
      $('#blog_submit').attr("disabled", false);
      var errors = data.responseJSON;
      if ($.isEmptyObject(errors) == false) {
        $.each(errors.errors, function (key, value) {
          $('.' + key)
          .closest('.form-group')
          .addClass('has-error')
          .append('<span class="help-block"><strong>' + value + '</strong></span>');
        });
      }
    });
  });
 });
</script>



<!-- Delete Blog -->
<script type="text/javascript">
  $(".deleteRecord").click(function(){
    var delete_blog = confirm("Do you want to Delete this blog ?");
    if(delete_blog == true) {
      var id = $(this).data("id");
      var token = $("meta[name='csrf-token']").attr("content");
      $.ajax(
      {
        url: "blogs/"+id,
        type: 'DELETE',
        data: {
          "id": id,
          "_token": token,
        },
        success: function (data){
          if(data['data']=='success'){
            window.location.reload(true);
          }else{
            alert('You must have at least one blog.!');
          }
        },
        error: function (response) {
          alert('something wrong !');
        }
      });
      return true;
    }else{
      return false;
    }
  });
</script>

<!-- check filesize textlimit and textarea lenght -->
<script type="text/javascript">
  $(document).ready(function(){

    $("#blog_item_image").change(function(){
      filesize(this);
    });
    $("#section_header").keyup(function(){
      var textlable=document.getElementById("textlimit-header");
      textlimit(this,30,textlable);
    });
    $('#section_title').keyup(function(){
      var textlable=document.getElementById("textlimit-title");
      textlimit(this,60,textlable);
    });
    $('#blog_item_content_heading').keyup(function(){
      var textlable=document.getElementById("textlimit-itemheading");
      textlimit(this,70,textlable);
    });
    $('#auther').keyup(function(){
      var textlable=document.getElementById("textlimit-auther");
      textlimit(this,20,textlable);
    });
    $('.blog_item_content_heading').keyup(function(){
      var textlable=document.getElementById("textlimit-itemheading_add");
      textlimit(this,70,textlable);
    });
    $('.auther').keyup(function(){
      var textlable=document.getElementById("textlimit-auther_add");
      textlimit(this,20,textlable);
    });
    $(document).on('click', '.closebtn', function(){
      modelClose();
    });
  });
</script>

@stop