<div class="modal fade" id="blog_model">
  <div class="modal-dialog">
    <div class="modal-content">
      {{ html()->form()->id('blogsForm')->acceptsFiles()->open() }} {{ csrf_field() }} @method('PATCH')
      <div class="modal-header">
        <h4 class="modal-title">Blog</h4>
        <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group section_header_model">
          {{ html() ->input('text', 'section_header') ->class('form-control section_header_model') ->id('section_header') ->placeholder('Enter Section Header') }}
          <div id="textlimit-header" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="form-group section_title_model">
          {{ html() ->input('text', 'section_title') ->class('form-control section_title') ->id('section_title') ->placeholder('Enter Section heading') }}
          <div id="textlimit-title" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="blog_item_model">
          <div class="form-group">
            {{ html() ->input('text', 'blog_item_content_heading') ->class('form-control blog_item_content_heading') ->id('blog_item_content_heading')
            ->placeholder('Enter blog heading') }}
            <div id="textlimit-itemheading" class="textlimit">
              <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
            </div>
          </div>
          <br><br>
          <div class="form-group">
            {{ html() ->textarea('blog_item_content_description') ->class('form-control blog_item_content_description summernote') ->id('blog_item_content_description')
            ->placeholder('Enter blog description') }}
          </div>
          <div class="form-group">
            {{ html() ->input('text', 'auther') ->class('form-control auther') ->id('auther') ->placeholder('Enter Auther Name') }}
            <div id="textlimit-auther" class="textlimit">
              <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
            </div>
          </div>
          <div class="form-group">

            {{ html()->file('blog_item_image') ->style(['border' => '1']) ->class('form-control blog_item_image') ->id('blog_item_image')
            ->attribute('value',asset('uploads/blogs/'.$blog->blog_item_image)) }}
          </div>
        </div>

      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="blog_id">
        <button type="submit" class="btn btn-primary" id="blog_submit">Submit</button>
        <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
      </div>
      <!-- </form> -->
      {{ html()->form()->close() }}
    </div>
  </div>
</div>
</div>