<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
 <meta name="csrf-token" content="{{ csrf_token() }}">
<title>Hoax Developers</title>
<script src="{{ asset('js/app.js') }}" defer></script>
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="stylesheet" href="http://9brainz.store/css/facts/bootstrap.min.css">
<link href="http://9brainz.store/css/facts/app.css" rel="stylesheet">
<style type="text/css">
h1, h2,h4, h5, h6,body{
font-family:Candara !important;
}
.d-none{
	display: block !important;
}
/*.full-height{
height: 100vh;
}*/
</style>
</head>
<body>
<div class="app">
<div class="authentication">
<div class="sign-in">
<div class="row no-mrg-horizon">
<div class="col-md-8 no-pdd-horizon d-none d-md-block">
<div class="full-height bg" style="background-image: url('defaultimage/blog-big-defualt.jpg')">
</div>
</div>
<div class="col-md-4 no-pdd-horizon">
<div class="table-container">
<div class="table-center">
<div class="full-height bg-white height-100">
<div class="vertical-align full-height pdd-horizon-70">
<div class="table-cell">
	<div class="pdd-horizon-15">
		<h1 style="color:#45c5ed;">Hoax developers</h1>
		<h3>Login</h3>
		<p class="mrg-btm-15 font-size-13">Please enter your user name and password to login</p>
		<form method="POST" action="{{ route('login') }}">
			@csrf
			<div class="form-group">
				<input placeholder="E-mail" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
				name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('email') }}</strong>
				</span> @endif
			</div>
			<div class="form-group">
				<input placeholder="Password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
				name="password" required> @if ($errors->has('password'))
				<span class="invalid-feedback" role="alert">
					<strong>{{ $errors->first('password') }}</strong>
				</span> @endif
			</div>
			<div class="form-group">
				<input type="checkbox" name="remember" id="remember" {{ old( 'remember') ? 'checked' : '' }}>
				<label for="agreement">Keep Me Signed In</label>
			</div>
			<button type="submit" class="btn btn-info" style="background-color:#45c5ed; border: none;">
						{{ __('Login') }}
			</button>
			<br>
			<a href="{{ route('password.request') }}">
						{{ __('Forgot Your Password?') }}
		</form>
		</div>
	</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>