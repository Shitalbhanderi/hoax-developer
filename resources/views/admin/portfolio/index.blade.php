@extends('admin.layout.index')
@section('title') Portfolio
@stop
@section('page-css')
<link rel="stylesheet" href="/assets/css/main.css">

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">


<style type="text/css">
.text-block h2 {
color: #5B79EE;
font-weight: 600;
margin-bottom: 5px;
}
</style>

@stop
@section('active-menu-icon')
<em class="fa fa-bar-chart">&nbsp;</em>
@stop
@section('active-menu') Portfolio
@stop
@section('page-header') Portfolio
@stop
@section('content')


<section id="portfolio" class="section-padding">
<!-- <div class="container"> -->
<div class="section-header text-center">
  <h5 class="wow fadeInDown">
    {{$portfolios[0]->section_header}}
    <a href="" id="1" data-toggle="modal" class="section_header_edit"> <i class='far fa-edit'></i></a>
  </h5>
  <h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
    {{$portfolios[0]->section_title}}
    <a href="" id="1" data-toggle="modal" class="section_title_edit"> <i class='far fa-edit'></i></a>
  </h1>
</div>
<div class="row">
  @php $count=$add=3;
  @endphp @foreach($portfolios as $key=>$portfolio) @if($key+1 == $count)
  <div class="col-xs-12 col-sm-12 col-lg-6 col-md-12">
    @php $portfolio_image=$portfolio->portfolio_item_image_big_url; $add=($add == 1)?5:1; $count += $add;
    @endphp @else
    <div class="col-xs-12 col-sm-12 col-lg-3 col-md-6">
      @php $portfolio_image=$portfolio->portfolio_item_image_small_url;
      @endphp @endif

      <a href="" id="{{$portfolio->id}}" data-toggle="modal" class="portfolio_item_edit" style="float: right;"> <i class='far fa-edit'></i></a>
      <div class="work-item wow fadeInLeft data-wow-delay="0.3s">
        <img src="{{$portfolio_image}}" class="image work-item-img">
        <div class="text-block">
          <h2 style="color:{{$portfolio->portfolio_item_name_color}}">{{$portfolio->portfolio_item_name}}</h2>
        </div>
        <div class="work-item-store">
          <div class="item-icon">
            @if($portfolio->portfolio_item_andriod)
            <a href="{!! url($portfolio->portfolio_item_andriod_url) !!}" target="_blank">
              <i class="hovicon effect-1 sub-a">
                <i class='fab fa-google-play'></i>
              </i>
            </a>             
            @endif @if($portfolio->portfolio_item_ios)
            <a href="{!! url($portfolio->portfolio_item_ios_url) !!}" target="_blank">
              <i class="hovicon effect-1 sub-a">
                <i class='fab fa-app-store-ios'></i>
              </i>
            </a> 
            @endif
          </div>
        </div>
      </div>
    </div>

    @endforeach
  </div>
</section>
@include('admin.portfolio.edit')
@stop

@section('page-js')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js"></script> 

<!-- color picker -->
<script>
   $('#portfolio_item_name_color').colorpicker({});
    $('.colorpicker').colorpicker({});
</script>

<!-- Open Particular fields to edit and summernote -->
<script>
  $(document).ready(function(){
    $(".section_header_edit").click(function(){
     $(".portfolio_item_model, .section_title_model").hide();
     $(".section_header_model").show();
   })
    $(".section_title_edit").click(function(){
     $(".portfolio_item_model, .section_header_model").hide();
     $(".section_title_model").show();
   });
    $(".portfolio_item_edit").click(function(){
      $(".section_header_model, .section_title_model").hide();
      $(".portfolio_item_model").show();
    });
    $('.android').click(function(){
     ($('.android').is(":checked"))?$(".portfolio_item_andriod_url").show():$(".portfolio_item_andriod_url").hide();
   });
    $('.ios').click(function(){
      ($('.ios').is(":checked"))?$(".portfolio_item_ios_url").show():$(".portfolio_item_ios_url").hide();
    })
  });

</script>

<!-- retrive data from database and show in model-->
<script>
  $(document).on('click', '.portfolio_item_edit,.section_header_edit,.section_title_edit', function(){
    var id = $(this).attr("id");
    $.ajax({
      url:"{{route('portfolio.show',$portfolio->id)}}",
      method:'get',
      data:{id:id},
      dataType:'json',
      success:function(data)
      {
        $('#section_header').val(data.section_header);
        $('#section_title').val(data.section_title);
        $('#portfolio_item_name').val(data.portfolio_item_name);
        $('#portfolio_item_andriod_url').val(data.portfolio_item_andriod_url);
        $('#android').prop('checked',data.android);
        $('#ios').prop('checked',data.ios);
        $('#portfolio_item_ios_url').val(data.portfolio_item_ios_url);
        $('#portfolio_item_name_color').val(data.portfolio_item_name_color);
        $('#portfolio_model').modal('show');
        $('#portfolio_id').val(id);
        
        ($('#android').is(":checked"))?$(".portfolio_item_andriod_url").show():$(".portfolio_item_andriod_url").hide();
        ($('#ios').is(":checked"))?$(".portfolio_item_ios_url").show():$(".portfolio_item_ios_url").hide();
        var textlable=document.getElementById("textlimit-header");
        textlimit('#section_header',30,textlable);
        var textlable=document.getElementById("textlimit-title");
        textlimit('#section_title',60,textlable);
        var textlable=document.getElementById("textlimit-itemname");
        textlimit('#portfolio_item_name',30,textlable);
      }
    })
  });
</script>

<!-- update data -->
<script>
  $(document).ready(function(){
   $('#portfolioForm').on('submit', function(event){
    var id = $('#portfolio_id').val();
    $('#portfolio_submit').attr("disabled", false);
    modelClose();
    $("#portfolio_submit").attr('disabled', 'disabled');
    event.preventDefault();
    $.ajax({
     url:'portfolio/'+id,
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success: function (data) {
     },
     error: function (response) {
     },
     complete: function(){
      $('#portfolio_submit').attr("disabled", false);
    }
  })
    .done(function(data) {
      window.location.reload(true);
    })
    .fail(function(data){
      $('#portfolio_submit').attr("disabled", false);
      var errors = data.responseJSON;
      if ($.isEmptyObject(errors) == false) {
        $.each(errors.errors, function (key, value) {
         $('#' + key)
         .closest('.form-group')
         .addClass('has-error')
         .append('<span class="help-block"><strong>' + value + '</strong></span>');
       });
      }
    });
  });
 });
</script>

<!-- check filesize textlimit and textarea lenght -->
<script type="text/javascript">
$(document).ready(function(){
  $("#portfolio_item_image").change(function(){
    filesize(this);
  });
  $("#section_header").keyup(function(){
    var textlable=document.getElementById("textlimit-header");
    textlimit(this,30,textlable);
  });
  $('#section_title').keyup(function(){
    var textlable=document.getElementById("textlimit-title");
    textlimit(this,60,textlable);
  });
  $('#portfolio_item_name').keyup(function(){
    var textlable=document.getElementById("textlimit-itemname");
    textlimit(this,30,textlable);
  });
  $(document).on('click', '.closebtn', function(){
    modelClose();
  });
});

</script>




@stop