<div class="modal fade" id="portfolio_model">
  <div class="modal-dialog">
    <div class="modal-content">
      {{ html()->form()->id('portfolioForm')->acceptsFiles()->open() }} {{ csrf_field() }} @method('PATCH')
      <div class="modal-header">
        <h4 class="modal-title">Portfolio</h4>
        <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group section_header_model">
          {{ html() ->input('text', 'section_header') ->class('form-control section_header') ->id('section_header') ->placeholder('Enter Section Header') }}
          <div id="textlimit-header" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="form-group section_title_model">
          {{ html() ->input('text', 'section_title') ->class('form-control section_title') ->id('section_title') ->placeholder('Enter Section Title') }}
          <div id="textlimit-title" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="portfolio_item_model">
          <div class="form-group">
            {{ html()->file('portfolio_item_image') ->style(['border' => '1']) ->class('form-control portfolio_item_image') ->id('portfolio_item_image')
            ->attribute('value',asset('uploads/portfolio/'.$portfolio->portfolio_item_image)) }}
          </div>
          <div class="form-group">
            {{ html() ->input('text', 'portfolio_item_name') ->class('form-control portfolio_item_name') ->id('portfolio_item_name')
            ->placeholder('Enter Application Name ') }}
          </div>
          <div id="textlimit-itemname" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
          <div class="form-group">
            {{ html() ->input('text', 'portfolio_item_name_color') ->class('form-control portfolio_item_name_color') ->id('portfolio_item_name_color')
            ->placeholder('Enter Color') }}
          </div>
         
          <div class="form-group">
            {{ html()->checkbox('android') ->class('android') }}
            <label style="display: inline-flex" for="is_unlimited_trials">Android</label> 
            {{ html() ->input('text', 'portfolio_item_andriod_url')
            ->class('form-control portfolio_item_andriod_url') ->id('portfolio_item_andriod_url') ->placeholder('Enter application Url For Android') }}
          </div>
          <div class="form-group">
            {{ html()->checkbox('ios') ->class('ios') }}
            <label style="display: inline-flex" for="is_unlimited_trials">iOS</label> {{ html() ->input('text', 'portfolio_item_ios_url')
            ->class('form-control portfolio_item_ios_url') ->id('portfolio_item_ios_url') ->placeholder('Enter application Url For iOS') }}
          </div>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="portfolio_id">
        <button type="submit" class="btn btn-primary" id="portfolio_submit">Submit</button>
        <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
      </div>
      <!-- </form> -->
      {{ html()->form()->close() }}
    </div>
  </div>
</div>