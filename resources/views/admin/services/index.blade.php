@extends('admin.layout.index')
@section('title') Services
@stop
@section('page-css')
<link rel="stylesheet" href="/assets/css/main.css">
@stop
@section('active-menu-icon')
<em class="fa fa-calendar"></em>

@stop
@section('active-menu') Services
@stop
@section('page-header') Services
@stop
@section('content')
<section id="services" class="section-padding">
  <div class="section-header text-center">
    <h5 class="wow fadeInDown">
      {{$services[0]->section_header}}
      <a href="" id="1" data-toggle="modal" class="section_header_edit"> <i class='far fa-edit'></i></a>
    </h5>
    <h1 class="section-title wow fadeInDown" data-wow-delay="0.3s">
      {{$services[0]->section_title}}
      <a href="" id="1" data-toggle="modal" class="section_title_edit"> <i class='far fa-edit'></i></a>
    </h1>
  </div>

  <div class="row">
    @foreach($services as $key => $service)
    <div class="col-md-6 col-lg-4 col-xs-12">
      <div class="services-item wow fadeInRight service-item-{{$service->id}}" data-wow-delay="0.3s">
        <a href="" id="{{$service->id}}" data-toggle="modal" class="services_item_edit" style="float: right;"> <i class='far fa-edit' ></i></a>
        <div class="services-item-icon">
          <i class="">
           <div class="service-item-img">
            @include('Icons.'.$service->id)
            <img id="" class="" height="50px" width="50px" src={{$service->service_item_icon_url}} />
          </div>
        </i>
      </div>
      <div class="services-content">
        <h3><a>{{$service->service_item_content_heading}}</a>
        </h3>
        <div>
          <p>{{$service->service_item_content_description}}</p>
        </div>
      </div>
    </div>
  </div>
  @include('admin.services.edit') @endforeach
</div>
</section>


@stop
@section('page-js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<!-- Open Particular fields to edit and summernote -->
<script>
  $(document).ready(function(){
    $(".section_header_edit").click(function(){
     $(".service_item_model, .section_title_model").hide();
     $(".section_header_model").show();
   });

    $(".section_title_edit").click(function(){
     $(".service_item_model, .section_header_model").hide();
     $(".section_title_model").show();
   });

    $(".services_item_edit").click(function(){
      $(".section_header_model, .section_title_model").hide();
      $(".service_item_model").show();
    });
  });

</script>

<!-- retrive data from database and show in model-->
<script>
  $(document).on('click', '.services_item_edit,.section_header_edit,.section_title_edit', function(){
    var id = $(this).attr("id");
    $.ajax({
      url:"{{route('services.show',$service->id)}}",
      method:'get',
      data:{id:id},
      dataType:'json',
      success:function(data)
      {
        $('#section_header').val(data.section_header);
        $('#section_title').val(data.section_title);
        $('#service_item_content_heading').val(data.service_item_content_heading);
        $('#service_item_content_description').val(data.service_item_content_description);
        $('#services_model').modal('show');
        $('#service_id').val(id);
        var textlable=document.getElementById("textlimit-header");
        textlimit('#section_header',30,textlable);
        var textlable=document.getElementById("textlimit-title");
        textlimit('#section_title',60,textlable);
        var textlable=document.getElementById("textlimit-itemheading");
        textlimit('#service_item_content_heading',30,textlable);
        var textlable=document.getElementById("textlimit-itemdescription");
        textlimit('#service_item_content_description',130,textlable);
        service_item_content_description=document.getElementById("service_item_content_description");
        textarearow(service_item_content_description);
      }
    })
  });
</script>

<!-- update data -->
<script>
  $(document).ready(function(){
   $('#servicesForm').on('submit', function(event){
    var id = $('#service_id').val();
    $("#service_submit").attr('disabled', 'disabled');
    modelClose();
    event.preventDefault();
    $.ajax({
     url:'services/'+id,
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success: function (data) {
     },
     error: function (response) {
     },
     complete: function(){
      $('#service_submit').attr("disabled", false);
    }
  })
    .done(function(data) {
     window.location.reload(true);
   })
    .fail(function(data){
      $('#service_submit').attr("disabled", false);
      var errors = data.responseJSON;
      if ($.isEmptyObject(errors) == false) {
        $.each(errors.errors, function (key, value) {
         $('#' + key)
         .closest('.form-group')
         .addClass('has-error')
         .append('<span class="help-block"><strong>' + value + '</strong></span>');
       });
      }
    });
  });
 });
</script>

<!-- check filesize textlimit and textarea lenght -->
<script type="text/javascript">
  $(document).ready(function(){
    $("#service_item_icon").change(function(){
      filesize(this);
    });
    $("#section_header").keyup(function(){
      var textlable=document.getElementById("textlimit-header");
      textlimit(this,30,textlable);
    });
    $('#section_title').keyup(function(){
      var textlable=document.getElementById("textlimit-title");
      textlimit(this,60,textlable);
    });
    $('#service_item_content_heading').keyup(function(){
      var textlable=document.getElementById("textlimit-itemheading");
      textlimit(this,30,textlable);
    });
    $('#service_item_content_description').keyup(function(){
      textarearow(this);
      var textlable=document.getElementById("textlimit-itemdescription");
      textlimit(this,130,textlable);
    });
    $(document).on('click', '.closebtn', function(){
      modelClose();
    });
  });
</script>

@stop