<div class="modal fade" id="services_model">
  <div class="modal-dialog">
    <div class="modal-content">

      {{ html()->form()->id('servicesForm')->acceptsFiles()->open() }} {{ csrf_field() }} @method('PATCH')
      <div class="modal-header">
        <h4 class="modal-title">Service</h4>
        <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <!-- <div id="has-error"></div> -->
        <div class="form-group section_header_model">
          {{ html() ->input('text', 'section_header') ->class('form-control section_header') ->id('section_header') ->placeholder('Enter Section Header') }}
          <div id="textlimit-header" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="form-group section_title_model">
          {{ html()->input('text', 'section_title')->class('form-control section_title')->id('section_title') ->placeholder('Enter Section Title') }}
          <div id="textlimit-title" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="service_item_model">
          <div class="form-group">
            {{ html()->file('service_item_icon') ->style(['border' => '1']) ->class('form-control service_item_icon') ->id('service_item_icon')
            ->attribute('value',asset('uploads/services/'.$service->service_item_icon)) }}
          </div>
          <div class="form-group">
            {{ html() ->input('text', 'service_item_content_heading', old('service_item_content_heading', $service->service_item_content_heading))
            ->class('form-control service_item_content_heading') ->id('service_item_content_heading') ->placeholder('Enter item heading') }}
            <div id="textlimit-itemheading" class="textlimit">
              <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
            </div>
          </div>
          <div class="form-group">
            {{ html() ->textarea('service_item_content_description', old('service_item_content_description', $service->service_item_content_description))
            ->class('form-control service_item_content_description') ->id('service_item_content_description') ->placeholder('Enter item description') }}
            <div id="textlimit-itemdescription" class="textlimit">
              <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="service_id">
        <button type="submit" class="btn btn-primary" id="service_submit" class="service_submit_btn">Submit</button>
        <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
      </div>
      <!-- </form> -->
      {{ html()->form()->close() }}
    </div>
  </div>
</div>