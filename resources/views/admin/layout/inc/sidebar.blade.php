<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
	<div class="profile-sidebar">
		<div class="profile-userpic">
			<img src="/defaultimage/user-defualt.jpg" class="img-responsive" alt="">
		</div>
		<div class="profile-usertitle">
			<div class="profile-usertitle-name"> {{ Auth::user()->name }}</div>
			<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="divider"></div>
	<form role="search">
		<div class="form-group">
			<!-- <input type="text" class="form-control" placeholder="Search"> -->
		</div>
	</form>
	<ul class="nav menu" id="nav-menu-header">
		<li class="nav-link {{(isset($route) && ($route=='welcome'))?'active':''}}">
			<a href="{{route('admin.welcome')}}"><em class="fa fa-dashboard">&nbsp;</em> Welcome</a>
		</li>
		<li class="nav-link {{(isset($route) && ($route=='home'))?'active':''}}">
			<a href="{{route('home.index')}}"><em class="fas fa-home">&nbsp;</em> Home</a>
		</li>
		<li class="nav-link {{(isset($route) && ($route=='service'))?'active':''}}">
			<a href="{{route('services.index')}}"><em class="fa fa-calendar">&nbsp;</em> Services</a>
		</li>
		<li class="nav-link {{(isset($route) && ($route=='portfolio'))?'active':''}}">
			<a href="{{route('portfolio.index')}}"><em class="fa fa-bar-chart">&nbsp;</em> Portfolio</a>
		</li>
		<!-- <li class="nav-link {{(isset($route) && ($route=='testimonial'))?'active':''}}">
			<a href="#"><em class="fa fa-toggle-off">&nbsp;</em> Testimonial</a>
		</li> -->
		<li class="nav-link {{(isset($route) && ($route=='blog'))?'active':''}}">
			<a href="{{route('blogs.index')}}"><em class="fa fa-clone">&nbsp;</em> Blog</a>
		</li>
		<li class="parent {{(isset($route) && ($route=='contactus' || ($route=='inquiry')))?'collapseopen':''}}" id="collapse_menu">
			<a data-toggle="collapse" href="#sub-item-1" id='collapse_menu_a'>
				<em class="fa fa-navicon">&nbsp;</em> Contact <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
			</a>
			<ul class="children collapse" id="sub-item-1">
				<li class="nav-lin">
					<a class="{{(isset($route) && ($route=='contactus'))?'active':''}}" href="{{route('contact.index')}}">
						<span class="fa fa-arrow-right">&nbsp;</span> Contact Us
					</a></li>
				<li class="nav-link">
					<a class="{{(isset($route) && ($route=='inquiry'))?'active':''}}" href="{{route('contact.inquiry')}}">
					<span class="fa fa-arrow-right">&nbsp;</span> Inquiries
					</a>
				</li>
			</ul>
		</li>
	</ul>

	</div>
<!--/.sidebar-->