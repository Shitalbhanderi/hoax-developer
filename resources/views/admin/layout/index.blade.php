	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="/defaultimage/title-logo.png" type="image/png" sizes="16x16">
		<title>Hoax Developers -@yield('title')</title>
		@yield('page-css')

		<link href="/admin/css/bootstrap.min.css" rel="stylesheet">

		<link href="/admin/css/font-awesome.min.css" rel="stylesheet">
		<link href="/admin/css/datepicker3.css" rel="stylesheet">
		<link href="/admin/css/styles.css" rel="stylesheet">
		<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css" rel="stylesheet">
		<!--Custom Font-->
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

		<link rel="stylesheet" href="/assets/fonts/line-icons.css">
		<!-- Owl carousel -->
		<link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
		<link rel="stylesheet" href="/assets/css/owl.theme.css">

		<link rel="stylesheet" href="/assets/css/magnific-popup.css">
		<link rel="stylesheet" href="/assets/css/nivo-lightbox.css">
		<!-- Animate -->
		<link rel="stylesheet" href="/assets/css/animate.css">
		<!-- Main Style -->
		<!-- Responsive Style -->
		<link rel="stylesheet" href="/assets/css/responsive.css">
		<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
		crossorigin='anonymous'>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


		<style type="text/css">
		.navbar-brand {
			padding: 0px;
		}
		input[type="file"]{
			height: auto;
		}
		.navbar .container-fluid .navbar-brand {
			margin-left: 10px;
		}
		.navbar-toggle {
			background: rgba(255, 255, 255, 0.1);
			padding: 10px;
			margin-top:0px;
		}
		.setting-btn{
			margin-top: -4px;
		}
		.dropdown a.dropdown-toggle{
			padding-top: 9px;
		}
	</style>

</head>

<body>

	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
					<a class="navbar-brand" href="#"><img src="/assets/img/Logo.png" height="38px" width="112px"></a>


					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown setting-btn">
							<a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
								<i class="material-icons">settings</i></span>
							</a>
							<ul class="dropdown-menu dropdown-alerts">
								<li>
									<a href="#changePassword_model" data-toggle="modal">
										{{-- <a href="{{route('showChangePasswordForm')}}"> --}}
											<div><em class="fas fa-wrench"></em> Change Password
											</div>
										</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="{{ route('logout') }}" onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
										<div><em class="fa fa-power-off"></em> Log Out
										</div>
									</a>

									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										@csrf
									</form>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- /.container-fluid -->
		</nav>
		@include('admin.layout.inc.sidebar')
		@include('auth.changepassword')

		<div class="col-sm-12 col-md-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
			<div class="row" style="padding-top:0px;">
				<ol class="breadcrumb">
					<li>
						<a href="#">																							@yield('active-menu-icon')
						</a>
					</li>
					<li class="active">
						@yield('active-menu')

					</li>
				</ol>
			</div>

		<!-- <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header" style="color:black">
					@yield('page-header')
				</h1>
			</div>
		</div> -->
		@yield('content')

		<script src="/admin/js/jquery-1.11.1.min.js"></script>
		<script src="/admin/js/bootstrap.min.js"></script>
		<script src="/admin/js/chart.min.js"></script>
		<script src="/admin/js/chart-data.js"></script>
		<script src="/admin/js/easypiechart.js"></script>
		<script src="/admin/js/easypiechart-data.js"></script>
		<script src="/admin/js/bootstrap-datepicker.js"></script>
		<script src="/admin/js/custom.js"></script>

		​@yield('page-js')

		<script>
			function textarearow(textarea){
				var maxrows=10;
				var txt=textarea.value;
				var cols=textarea.cols;
				var arraytxt=txt.split('\n');
				var rows=arraytxt.length;
				for (i=0;i<arraytxt.length;i++)
					rows+=parseInt(arraytxt[i].length/cols);
				if (rows>maxrows) textarea.rows=maxrows;
				else textarea.rows=rows;
			}
			function filesize(file){
				$(file).closest('.form-group').find('.help-block').remove();
				$(file).closest('.form-group').removeClass('has-error');
				if(file.files[0].size > 2097152){
					$(file).closest('.form-group')
					.addClass('has-error')
					.append('<span class="help-block"><strong>' + "Image size must be less than 2 MB" + '</strong></span>');
					file.value = "";
				}
			}
			function textlimit(input,max_lenght,textlable){
				var length =$(input).val().length;
				$(textlable).find('#cur_text').text(length);
				$(textlable).find('#max_text').text(max_lenght);
				if(length > max_lenght) {
					$(textlable).closest('.form-group').addClass('has-error');
					$(textlable).css('color','red');
				}else{
					$(textlable).closest('.form-group').removeClass('has-error');
					$(textlable).css('color','black');
				}
			}
		</script>
		<!-- Change Password -->
		<script>
			$(document).ready(function(){
				$('#changePasswordForm').on('submit', function(event){
					$('form').find('.help-block').remove();
					$('form').find('.alert').removeClass('alert alert-danger');
					$('form').find('.form-group').removeClass('has-error');
					event.preventDefault();

					$.ajax({
						url:"{{route('changePassword')}}",
						method:"POST",
						data:new FormData(this),
						dataType:'JSON',
						contentType: false,
						cache: false,
						processData: false
					})
					.done(function(data) {
						if(data.success==0){
							$('#has-error').addClass('alert alert-danger').append('<span class="help-block"><strong>' + data.message + '</strong></span>');
						}else{
							window.location.reload(true);
						}

					})
					.fail(function(data){
						var errors = data.responseJSON;
						if ($.isEmptyObject(errors) == false) {
							$.each(errors.errors, function (key, value) {
								$('#' + key)
								.closest('.form-group')
								.addClass('has-error')
								.append('<span class="help-block"><strong>' + value + '</strong></span>');
							});
						}
					});
				});
			});
		</script>
		<script type="text/javascript">
			$(document).ready(function(){
				var collapse_menu=document.getElementById("collapse_menu");
				if($("#collapse_menu").hasClass("collapseopen")==true){
					$('#collapse_menu').find('.pull-right').remove();
					$('#collapse_menu').find('ul').addClass('in');
					$('#collapse_menu').find('#collapse_menu_a').append('<span data-toggle="collapse" href="#sub-item-1" class="icon pull-right" aria-expanded="true"><em class="fa fa-plus fa-minus"></em></span>');
				}
			});
		</script>
		<!-- remove error on click model close btn -->
		<script type="text/javascript">
			function modelClose(){
				$('form').find('.help-block').remove();
				$('form').find('.alert').removeClass('alert alert-danger');
				$('form').find('.form-group').removeClass('has-error');
			}
		</script>
		<script type="text/javascript">
			function submitData(id,submit_btn,url_route){
				var url_update=url_route+'/'+id;

				debugger;
				$('form').find('.help-block').remove();
				$('form').find('.alert').removeClass('alert alert-danger');
				$('form').find('.form-group').removeClass('has-error');
				$("#"+submit_btn).attr('disabled', 'disabled');
				event.preventDefault();
				$.ajax({
					url:url_route+'/'+id,
					method:"POST",
					data:new FormData(this),
					// dataType:'JSON',
					// contentType: false,
					// cache: false,
					// processData: false,
					// success: function (data) {
					// },
					// error: function (response) {
					// },
					// complete: function(){
					// 	$('#blog_submit').attr("disabled", false);
					// }
				})
				.done(function(data) {
					debugger;
					window.location.reload(true);
				})
				.fail(function(data){
					$("#"+submit_btn).attr("disabled", false);
					var errors = data.responseJSON;
					if ($.isEmptyObject(errors) == false) {
						$.each(errors.errors, function (key, value) {
							$('#' + key)
							.closest('.form-group')
							.addClass('has-error')
							.append('<span class="help-block"><strong>' + value + '</strong></span>');
						});
					}
				});
			}
		</script>

	</body>
	</html>