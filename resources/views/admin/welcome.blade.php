@extends('admin.layout.index')
@section('title') welcome
@stop
@section('page-css')
<link rel="stylesheet" href="/assets/fonts/line-icons.css">
<!-- Owl carousel -->
<link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/assets/css/owl.theme.css">

<link rel="stylesheet" href="/assets/css/magnific-popup.css">
<link rel="stylesheet" href="/assets/css/nivo-lightbox.css">
<!-- Animate -->
<link rel="stylesheet" href="/assets/css/animate.css">
<!-- Main Style -->
<link rel="stylesheet" href="/assets/css/main.css">

<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
 crossorigin='anonymous'>

<style type="text/css">
	p{
		font-size: 45px;
	}

</style>


@stop
@section('active-menu-icon')
<!-- <em class="fa fa-services"></em> -->
<em class="fa fa-dashboard">&nbsp;</em>

@stop
@section('active-menu')  welcome
@stop
@section('page-header') welcome
@stop
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<div class="text-center">
			<p>Welcome</p>
		</div>
		</div>
</div>














@stop
@section('page-js')