<div class="modal fade" id="contactus_model">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- 'POST',route('contact.update',$contact->id) -->
      {{ html()->form()->id('contactusForm')->acceptsFiles()->open() }}
      {{ csrf_field() }}
      @method('PATCH')
      <div class="modal-header">
        <h4 class="modal-title">Contact</h4>
        <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group logo_model">
          {{
            html()->file('logo')
            ->style(['border' => '1'])
            ->class('form-control logo')
            ->id('logo')
            ->attribute('value',asset('uploads/'.$contact->logo))
          }}
        </div>
        <div class="form-group footer_text_model">
          {{
            html()
            ->textarea('footer_text',old('footer_text', $contact->footer_text))
            ->class('form-control footer_text')
            ->id('footer_text')
            ->placeholder('Enter Text')
          }}
          <div id="textlimit-text" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
        </div>
        </div> 
        <div class="footer_link_model">
          <div class="form-group">
            {{
              html()
              ->input('text', 'skype_id',old('skype_id', $contact->skype_id))
              ->class('form-control skype_id')
              ->id('skype_id')
              ->placeholder('Enter skype Id')
            }}
            <div id="textlimit-skypeid" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
        </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="contactus_id">
        <button type="submit" class="btn btn-primary" id="contactus_submit">Submit</button>
        <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
      </div>
      {{ html()->form()->close() }}
    </div>
  </div>
</div> 

<!-- Social Media Icon -->

<div class="modal fade" id="socialmedia_model">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- 'POST',route('contact.update',$contact->id) -->
      {{ html()->form()->id('SocialMediaForm')->acceptsFiles()->open() }}
      {{ csrf_field() }}
      @method('PATCH')
      <div class="modal-header">
        <h4 class="modal-title">Social Media</h4>
        <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

        <div class="form-group footer_socialmedia_model">
          <div class="form-group">
            <button type="button" name="add" id="add" class="btn btn-success">Add</button>
          </div>
          <div id="has-error" class="has-error"></div>
          <div class="form-group" id="footer_socialmedia">
          </div>
        </div>
         
      </div>
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="socialmedia_id">
        <button type="submit" class="btn btn-primary" id="contactus_submit">Submit</button>
        <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
      </div>
      {{ html()->form()->close() }}
    </div>
  </div>
</div> 