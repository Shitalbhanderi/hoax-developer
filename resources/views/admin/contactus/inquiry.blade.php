@extends('admin.layout.index')

@section('title')
Contact Us
@stop
@section('page-css')
<link rel="stylesheet" href="/assets/fonts/line-icons.css">
<!-- Owl carousel -->
<link rel="stylesheet" href="/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/assets/css/owl.theme.css">

<link rel="stylesheet" href="/assets/css/magnific-popup.css">
<link rel="stylesheet" href="/assets/css/nivo-lightbox.css">
<!-- Animate -->
<link rel="stylesheet" href="/assets/css/animate.css">
<!-- Main Style -->
<link rel="stylesheet" href="/assets/css/main.css">

<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">


@stop

@section('active-menu-icon')
<em class="fa fa-address-book"></em>
@stop

@section('active-menu')
Inquiry
@stop

@section('page-header')
Inquiry
@stop

@section('content')
<section id="blog" class="section-padding">
    <div class="table-responsive">          
      <table class="table">
            {!! $html->table() !!}
      </table>
    </div>
</section>

@stop

@section('page-js')
<div class="" style="padding-top: 50px;"></div>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
{!! $html->scripts() !!}

@stop