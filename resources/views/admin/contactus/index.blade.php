@extends('admin.layout.index')
@section('title') Home
@stop
@section('page-css')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<link rel="stylesheet" href="/assets/css/main.css">

<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
crossorigin='anonymous'>
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
crossorigin='anonymous'>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">

<style type="text/css">
.linkclr {
color: #fff;
}
#footer{
overflow: hidden;
}

a:hover {
color: #fff;
}
</style>

@stop
@section('active-menu-icon')
<em class="fa fa-address-book"></em>
@stop
@section('active-menu') Contact Us
@stop
@section('page-header') Contact Us
@stop
@section('content')
<footer id="footer" class="footer-area section-padding">
<div class="container">
<div class="row">
<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 col-mb-12">
  <div class="widget">
    <h3 class="footer-logo">
      <img src={{$contact->logo_url}} alt="" height="55px" width="160px">
      <a href="" id="1" data-toggle="modal" class="linkclr logo_edit"> <i class='far fa-edit'></i></a>
    </h3>
    <div class="textwidget">
      <p>{{$contact->footer_text}}
        <a href="" id="1" data-toggle="modal" class="linkclr footer_text_edit"> <i class='far fa-edit'></i></a></p>
      </div>
      <div class="social-icon">
        <input type="hidden" id={{sizeof($sociamedias)}} name="arr_size" class="arr_size"> @foreach($sociamedias as $socialmedia)
        <a href="{!! url($socialmedia->url) !!}"  target="_blank"><img src={{$socialmedia->icon_url}}></a> @endforeach
        <a href="" id="1" data-toggle="modal" class="linkclr footer_socialmedia_edit"> <i class='far fa-edit'></i></a>
      </div>
    </div>
  </div>
  <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6">
    <h3 class="footer-titel">Sitemap</h3>
    <ul class="footer-link">
      <li><a href="/admin/services">Services</a></li>
      <li><a href="/admin/protfolio">Portfolio</a></li>
      <li><a href="/admin/blogs">Blog</a></li>
      <!-- <li><a href="#">Testimonials</a></li> -->
      <li><a href="/admin/contact">Contact</a></li>
    </ul>
  </div>
  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
    <h3 class="footer-titel">What We Provide</h3>
    <ul class="footer-link">
      @foreach($services as $service)
      <li>{{$service->service_item_content_heading}}</li>
      @endforeach
    </ul>
  </div>
  <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    <h3 class="footer-titel">Contact Us</h3>
    <ul class="footer-link">
      <li>
       <a href="mailto:hoaxdevelopers@gmail.com" target="_blank"> <i class='far fa-envelope'></i> hoaxdevelopers@gmail.com</a>
       </li>
        <li>
          <a href="skype:{{$contact->skype_id}}?chat"><i class='fab fa-skype'></i> {{$contact->skype_id}} </a><a href="" id="1" data-toggle="modal" class="linkclr footer_link_edit"
          style="color:#fff"> <i class='far fa-edit'></i></a></li>
        </ul>
    </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="text-center">
          <hr style="height:1px; width: 80%; border:none;  background-color:#808080;">
          <p>&copy;2019-2021 HOAX DEVELOPERS. ALL RIGHTS RESEVRED.</p>
        </div>
      </div>
    </div>
  </div>
</footer>
@include('admin.contactus.edit')
@stop
@section('page-js')
<!-- Open Particular fields to edit and summernote -->
<script>
  $(document).ready(function(){
    $(".logo_edit").click(function(){
      $('.footer_text_model,.footer_link_model,.footer_socialmedia_model').hide();
      $('.logo_model').show();
    });
    $(".footer_text_edit").click(function(){
      $('.logo_model, .footer_link_model,.footer_socialmedia_model').hide();
      $('.footer_text_model').show();
    });
    $(".footer_socialmedia_edit").click(function(){
      $('.logo_model, .footer_text_model,.footer_link_model').hide();
      $('.footer_socialmedia_model').show();
    });
    $(".footer_link_edit").click(function(){
      $('.logo_model, .footer_text_model,.footer_socialmedia_model').hide();
      $('.footer_link_model').show();
    });
  });
</script>

<!-- retrive data from database and show in model-->
<script>
  $(document).on('click', '.logo_edit,.footer_text_edit,.footer_link_edit', function(){
    var id = $(this).attr("id");
    $.ajax({
      url:"{{route('contact.show',$contact->id)}}",
      method:'get',
      data:{id:id},
      dataType:'json',
      success:function(data)
      {
        $('#footer_text').val(data.footer_text);
        $('#skype_id').val(data.skype_id);
        $('#contactus_model').modal('show');
        $('#contactus_id').val(id);
        var textlable=document.getElementById("textlimit-text");
        textlimit("#footer_text",230,textlable);
        var textlable=document.getElementById("textlimit-skypeid");
        textlimit("#skype_id",50,textlable);
      }
    })
  });
</script>

<!-- update data -->
<script>
  $(document).ready(function(){
   $('#contactusForm').on('submit', function(event){
    var id = $('#contactus_id').val();
    $("#contactus_submit").attr('disabled', 'disabled');
    $('form').find('.help-block').remove();
    $('form').find('.alert').removeClass('alert alert-danger');
    $('form').find('.form-group').removeClass('has-error');
    event.preventDefault();
    $.ajax({
     url:'contact/'+id,
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success: function (data) {
     },
     error: function (response) {
     },
     complete: function(){
      $('#contactus_submit').attr("disabled", false);
    }
  })
    .done(function(data) {
     window.location.reload(true);
   })
    .fail(function(data){
     $('#contactus_submit').attr("disabled", false);
     var errors = data.responseJSON;
     if ($.isEmptyObject(errors) == false) {
      $.each(errors.errors, function (key, value) {
        $('#' + key)
        .closest('.form-group')
        .addClass('has-error')
        .append('<span class="help-block"><strong>' + value + '</strong></span>');
      });
    }
  });
  });
 });
</script>

<!-- add dynamic input fileds for social medias -->
<script>
  $(document).ready(function(){
    var postURL = "<?php echo url('addmore'); ?>";
    var arr_size=$('.arr_size').attr("id");
    var i=arr_size;
    $('#add').click(function(){
      if(i<5){
       i++;
       $('#footer_socialmedia').append('<div id="row'+i+'" class="dynamic-added"><input type="hidden" name="socialmedia['+i+'][id]" value=""><div class="form-group"><input type="file" name="socialmedia['+i+'][logo]" class="form-control" id="socialmedia_logo_'+i+'"/></div><div class="form-group"><input type="text" name="socialmedia['+i+'][link]" placeholder="Enter Url" class="form-control" id="socialmedia_link_'+i+'"/></div><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button>');
     }
   });
    $(document).on('click', '.btn_remove', function(){
     var button_id = $(this).attr("id");
     $('#row'+button_id+'').remove();
     i--;
   });
  });

</script>

<!-- retrive data of social media and create dynamic fields for social media  -->
<script>
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $(document).on('click', '.footer_socialmedia_edit', function(){
    var s_id = $(this).attr("id");
    $.ajax({
      url:"{{route('contact.showSocialMedia')}}",
      method:'get',
      dataType:'json',
      success:function(response)
      {
        if(response['data'] != null){
         len = response['data'].length;
       }
       for(var i=0; i<len; i++){
         var id = response['data'][i].id;
         var link = response['data'][i].url;
         var icon = response['data'][i].icon;
         var tr_str = '<div id="row'+id+'" class="dynamic-added"><input type="hidden" name="socialmedia['+i+'][id]" value="'+id+'" id="socialmedia['+i+'][id]"><div class="form-group" id="1"><input type="file" name="socialmedia['+i+'][logo]" class="form-control" id="socialmedia_logo_'+i+'"/></div><div class="form-group" id="1"><input type="text" name="socialmedia['+i+'][link]" placeholder="Enter Url" class="form-control" value="'+link+'" id="socialmedia_link_'+i+'"/></div><button type="button" name="remove" id="'+id+'" class="btn btn-danger btn_remove">X</button>';
         $("#footer_socialmedia").append(tr_str);
       }

       $('#socialmedia_model').modal('show');
       $('#socialmedia_id').val(s_id);
     }
   })
    $(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");
      var token = $("meta[name='csrf-token']").attr("content");
      $.ajax(
      {
        url: "contact-deletesocialmedia/"+button_id,
        type: 'DELETE',
        data: {
          "_token": token,
        },
        success: function (){
         $('#row'+button_id+'').remove();
       }
     });
    });
  });
</script>



<!-- update social media data -->
<script>
$(document).ready(function(){
 $('#SocialMediaForm').on('submit', function(event){
  $("#socialmedia_submit").attr('disabled', 'disabled');
  modelClose();
  event.preventDefault();
  $.ajax({
   url:'contact-updatesocialmedia',
   method:"POST",
   data:new FormData(this),
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success: function (data) {
   },
   error: function (response) {
   },
   complete: function(){
    $('#contactus_submit').attr("disabled", false);
  }
})
  .done(function(data) {
   window.location.reload(true);
 })
  .fail(function(data){
    var errors = data.responseJSON;
    if ($.isEmptyObject(errors) == false) {
      $('#contactus_submit').attr("disabled", false);
      $.each(errors.errors, function (key, value) {
      var array = key.split(".");
      var name='socialmedia_'+array[2]+'_'+array[1];
      $('#'+name)
      .closest('.form-group')
      .addClass('has-error')
      .append('<span class="help-block"><strong>' + value + '</strong></span>');
      var uploadField = document.getElementById(name);
          if(uploadField.files[0].size > 2097152){
            $(uploadField).closest('.form-group')
            .addClass('has-error')
            .append('<span class="help-block"><strong>' + "Image size must be less than 2 MB" + '</strong></span>');
            uploadField.value = "";

          }
      });
    }
  });
});
});

</script>
<!-- Check file size -->
<script type="text/javascript">
var uploadField = document.getElementById("logo");
uploadField.onchange = function() {
$(this).closest('.form-group').find('.help-block').remove();
$(this).closest('.form-group').removeClass('has-error');
if(this.files[0].size > 2097152){
 $(this).closest('.form-group')
 .addClass('has-error')
 .append('<span class="help-block"><strong>' + "Image size must be less than 2 MB" + '</strong></span>');
 this.value = "";
};
};
</script>

<!-- check filesize textlimit and textarea lenght -->
<script type="text/javascript">
$(document).ready(function(){
$("#logo").change(function(){
filesize(this);
});
$('#footer_text').keyup(function(){
textarearow(this);
var textlable=document.getElementById("textlimit-text");
textlimit(this,230,textlable);
});
$('#skype_id').keyup(function(){
var textlable=document.getElementById("textlimit-skypeid");
textlimit(this,50,textlable);
});
$(document).on('click', '.closebtn', function(){
modelClose();
});
});

</script>
@stop