@extends('admin.layout.index') 
@section('title') Home 
@stop 
@section('page-css')
<link rel="stylesheet" href="/assets/css/main.css">

<style type="text/css">
  .intro-img {
    width: 90%;
  }

  #hero-area {
    padding-top: 50px;
    padding-left: 20px;
  }

  .linkclr {
    color: #fff;
  }

  .head-title a {
    font-size: 15px;
  }

  .img-fluid {
    max-width: 100%;
    height: auto;
  }

  input:focus {
    border-color: blue;
  }
</style>






@stop 
@section('active-menu-icon')
<em class="fa fa-home"></em> 
@stop 
@section('active-menu') Home 
@stop 
@section('page-header') Home 
@stop 
@section('content')

<div id="hero-area" class="hero-area-bg">
  <!--   <div class="row"> 
      <a href="index.html" class="navbar-brand"><img src="/assets/img/logo.png"></a>
    </div> -->
  <div class="row">
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12" style="padding-left: 20px;">
      <div class="contents">
        <h1 class="head-title" style="word-wrap: break-word;">{{$home->head_title}}
          <a href="" id="1" data-toggle="modal" class="linkclr head_title_edit"> <i class='far fa-edit'></i></a></h1>
        <p class="herofont">{{$home->hero_description}}
          <a href="" id="1" data-toggle="modal" class="linkclr hero_description_edit"> <i class='far fa-edit'></i></a>
        </p>
        <div class="header-button">
          <a rel="nofollow" href="{!! url($home->header_button_link) !!}" target="_blank"><img src={{$home->header_button_url}}></a>
          <a href="" id="1" data-toggle="modal" class="linkclr header_button_and_link_edit"> <i class='far fa-edit'></i></a>
        </div>
      </div>
    </div>
    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
    <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
      <div class="intro-img">
        <a href="" id="1" data-toggle="modal" class="linkclr intro_img_edit" style="float: right;"> <i class='far fa-edit'></i></a>
        <img class="img-fluid" src={{$home->intro_img_url}}>
      </div>
    </div>
  </div>
</div>
  @include('admin.home.edit') 

@stop 
@section('page-js')

<!-- Open Particular fields to edit -->
<script>
  $(document).ready(function(){
    $(".head_title_edit").click(function(){
      $('.hero_description_model, .header_button_and_link_model,.intro_img_model,.addMoreHeaderButton').hide();
      $('.head_title_model').show();
    });
    $(".hero_description_edit").click(function(){
      $('.head_title_model, .header_button_and_link_model,.intro_img_model,.addMoreHeaderButton').hide();
      $('.hero_description_model').show();
    });
    $(".header_button_and_link_edit").click(function(){
      $('.head_title_model, .hero_description_model,.intro_img_model').hide();
      $('.header_button_and_link_model,.addMoreHeaderButton').show();

    });
    $(".intro_img_edit").click(function(){
      $('.head_title_model, .hero_description_model, .header_button_and_link_model,.addMoreHeaderButton').hide();
      $('.intro_img_model').show();
    });
  });

</script>

<!-- retrive data from database and show in model-->
<script>
  $(document).on('click', '.head_title_edit,.header_button_and_link_edit,.hero_description_edit,.intro_img_edit,.section_header_edit,.section_title_edit', function(){
    var id = $(this).attr("id");

    $.ajax({
      url:"{{route('home.show',$home->id)}}",
      method:'get',
      data:{id:id},
      dataType:'json',
      success:function(data)
      {
        $('#head_title').val(data.head_title);
        $('#hero_description').val(data.hero_description);
        $('#header_button_link').val(data.header_button_link);
        $('#home_model').modal('show');
        $('#home_id').val(id);    
        var textlable=document.getElementById("textlimit-title");
        textlimit("#head_title",35,textlable);
        var textlable=document.getElementById("textlimit-description");
        textlimit("#hero_description",250,textlable);
        var hero_description=document.getElementById("hero_description");
        textarearow(hero_description);   
      }
    })
     
  });
</script>

<!-- update data -->
<script>
  $(document).ready(function(){
   $('#homeForm').on('submit', function(event){
    $("#home_submit").attr('disabled', 'disabled');
    modelClose();
    event.preventDefault();
    $.ajax({
     url:"{{route('home.update',$home->id)}}",
     method:"POST",
     data:new FormData(this),
     dataType:'JSON',
     contentType: false,
     cache: false,
     processData: false,
     success: function (data) {
     },
     error: function (response) {
     },
    complete: function(){
        $('#home_submit').attr("disabled", false);
      }
   })
    .done(function(data) {
     window.location.reload(true);
   })
    .fail(function(data){
      $('#home_submit').attr("disabled", false);
     var errors = data.responseJSON;
     if ($.isEmptyObject(errors) == false) {
      $.each(errors.errors, function (key, value) {
        $('#' + key)
                .closest('.form-group')
                .addClass('has-error')
                .append('<span class="help-block"><strong>' + value + '</strong></span>');
      });
    }
  });
  });
 });
</script>

<!-- check filesize textlimit and textarea lenght -->
<script type="text/javascript">
  $(document).ready(function(){
    $("#intro_img").change(function(){
      filesize(this);
    }); 
    $("#header_button").change(function(){
      filesize(this);
    });
    $("#hero_description").keyup(function(){
      textarearow(this);
      var textlable=document.getElementById("textlimit-description");
      textlimit(this,250,textlable);
    });
    $('#head_title').keyup(function(){
      var textlable=document.getElementById("textlimit-title");
      textlimit(this,35,textlable);  
    });
     $(document).on('click', '.closebtn', function(){
      modelClose();
    });
});

</script>
@stop