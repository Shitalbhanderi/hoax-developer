<div class="modal fade" id="home_model">
  <div class="modal-dialog">
    <div class="modal-content">
      {{ html()->form()->id('homeForm')->acceptsFiles()->open() }} {{ csrf_field() }} @method('PATCH')
      <div class="modal-header">
        <h4 class="modal-title">Home</h4>
        <button type="button" class="close closebtn" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group head_title_model">
          {{ html() ->input('text', 'head_title', old('head_title', $home->head_title)) ->class('form-control head_title') ->id('head_title')
          ->placeholder('Enter header title') }}
          <div id="textlimit-title" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>

        <div class="form-group hero_description_model">
          {{ html() ->textarea('hero_description', old('hero_description', $home->hero_description)) ->class('form-control hero_description')
          ->id('hero_description') ->placeholder('Enter Hero Descrition') }}
          <div id="textlimit-description" class="textlimit">
            <label name="cur_text" id="cur_text"></label>/<label name="max_text" id="max_text"></label>
          </div>
        </div>
        <div class="header_button_and_link_model">
          <div class="form-group">
            {{ html()->file('header_button') ->style(['border' => '1']) ->class('form-control header_button') ->id('header_button') ->attribute('value',asset('uploads/home/'.$home->header_button))
            }}
          </div>
          <div class="form-group">
            {{ html() ->input('text', 'header_button_link', old('header_button_link', $home->header_button_link)) ->class('form-control
            header_button_link') ->id('header_button_link') ->placeholder('Enter Header Button Link') }}
          </div>
        </div>
        <div class="form-group intro_img_model">
          {{ html()->file('intro_img') ->style(['border' => '1']) ->class('form-control intro_img') ->id('intro_img') ->attribute('value',asset('uploads/home/'.$home->intro_img))
          ->attribute('title','abc')
          }}
         
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="hidden" id="home_id">
        <button type="submit" class="btn btn-primary" id="home_submit">Submit</button>
        <button type="button" class="btn btn-danger closebtn" data-dismiss="modal">Close</button>
      </div>
      {{ html()->form()->close() }}
    </div>
  </div>
</div>