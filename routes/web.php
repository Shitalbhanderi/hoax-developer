<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'index');


Route::view('staticindex', 'index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {


    Route::get('/welcome', function () {
        return view('admin.welcome', ['route' => 'welcome']);
    })->name('admin.welcome');

    Route::resource('home', 'HomeController', ['only' => ['index', 'show', 'update', 'destroy']]);
    Route::resource('services', 'ServicesController', ['only' => ['index', 'update', 'show']]);
    Route::resource('blogs', 'BlogsController');
    Route::resource('portfolio', 'PortfolioController', ['only' => ['index', 'update', 'show']]);
    Route::resource('contact', 'ContactUsController', ['only' => ['index', 'store', 'show', 'update']]);
    Route::get('contact-socialMedia', 'ContactUsController@showSocialMedia')->name('contact.showSocialMedia');
    Route::patch('contact-updatesocialmedia', 'ContactUsController@updatesocialmedia')->name('contact.updatesocialmedia');
    Route::delete('contact-deletesocialmedia/{id}', 'ContactUsController@deletesocialmedia')->name('contact.deletesocialmedia');
    Route::get('contact-inquiry', 'ContactUsController@inquieryIndex')->name('contact.inquiry');
});

Route::group(['namespace' => 'front'], function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::resource('contactus', 'ContactUsController', ['only' => ['index', 'store']]);
    Route::get('blog/{id}', 'HomeController@blogReadMore')->name('blog-readmore');
    Route::get('blogs', 'HomeController@blogViewAll')->name('blog-viewall');
    Route::get('privacy-policy', 'HomeController@privacypolicy');
    Route::get('terms-conditions', 'HomeController@termsconditions');
});

Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
    Route::get('/changePassword', 'HomeController@showChangePasswordForm')->name('showChangePasswordForm');
    Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');
});
